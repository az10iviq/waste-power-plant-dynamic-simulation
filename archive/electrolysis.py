
from import_setup import *
from init import *
from ..src.gases import *

@jitclass([("max_el_power_uptake", float64), ("electrolyzer_efficiency_rules", float64[:,:]), ("electrolyzer_stepless_intervals", float64[:,:]), ("capex_sp", float64), ("opex_factor", float64), ("h2_production_w_efficiencies", float64[:]), ("o2_production_w_efficiencies", float64[:]), ("o2_production_w_loads", float64[:]), ("h2o_consumption_w_efficiencies", float64[:])])
class Electrolyzer:
    h2: Gas
    h2o: Gas
    def __init__(self, max_el_power_uptake: float, electrolyzer_efficiency_rules: np.ndarray, electrolyzer_stepless_intervals: np.ndarray, h2: Gas, h2o: Gas, capex_sp: float, opex_factor: float,):
        """
        Class for Electrolyzer objects. It will be used to calculate the CAPEX, OPEX, H2 and O2 Production as well as Water consumption and surplus heat of the Electrolyzer.
        Arguments:
            eta_el_hu: float, electrical efficiency of the electrolyzer in the hydrogen utilization process. Value between 0 and 1.
            max_el_power_uptake: float, electrical power consumption of the electrolyzer in kW
            h2_ho: float, upper heating value of H2 in kWh/Nm^3
            h2_molar_weight: float, molar weight of H2 in kg/mol
            h2_density_norm: float, density of H2 at norm conditions in kg/Nm^3
            h2o_molar_weight: float, molar weight of H2O in kg/mol
            h2o_density: float, density of H2O in kg/m^3
            capex_sp: float, specific CAPEX of the electrolyzer in EURO/kW_el
            opex_factor: float, OPEX factor of the electrolyzer in EURO/kW_el/a
            TODO: add functionality for plant failure
        """
        self.max_el_power_uptake = max_el_power_uptake
        self.electrolyzer_efficiency_rules = electrolyzer_efficiency_rules
        self.electrolyzer_stepless_intervals = electrolyzer_stepless_intervals
        self.h2 = h2
        self.h2o = h2o
        self.capex_sp = capex_sp
        self.opex_factor = opex_factor
        # pre-calculate the o2 and h2 output for each efficiency rule
        self.h2_production_w_efficiencies = self.electrolyzer_efficiency_rules[:, 1] * self.max_el_power_uptake / h2.h_o
        self.o2_production_w_efficiencies = self.h2_production_w_efficiencies / 2
        # pre-calculate the o2 output for each load
        self.o2_production_w_loads = np.multiply(self.electrolyzer_efficiency_rules[:, 0], self.o2_production_w_efficiencies)
        # pre-calculate the liquid h2o consumption for each efficiency rule
        self.h2o_consumption_w_efficiencies = self.h2_production_w_efficiencies * self.h2.rho_norm / self.h2.M * self.h2o.M / self.h2o.rho_l
    
    def run(self, load: float, electrol_new_run_time: float):
        """
        Runs if the operator decides to run electrolyzer (load > 0).
        Calculates the thermal power output, h2 and o2-output as well as electricity and water consumption of the plant.
        """
        if load == 0:
            th_power, o2_production, h2_production, el_consumption, h2o_consumption = 0,0,0,0,0
        else:
            o2_production, h2_production, th_power = self.calculate_output(load)
            el_consumption, h2o_consumption = self.calculate_consumption(load)
        
        return load, o2_production, h2_production, th_power, el_consumption, h2o_consumption, electrol_new_run_time
    
    def calculate_output(self, load: float):
        """
        calculates o2 and h2 output as well as thermal power output based on the load of the electrolyzer.
        Arguments:
            load: float, load of the electrolyzer in kW_el
        Returns:
            o2_production: float, o2 production in Nm^3/h
            h2_production: float, h2 production in Nm^3/h
            th_power: float, thermal power output of surplus heat in kW_th
        """
        # Find closest rule to approximate conversion efficiency at specific load
        closest_rule = np.argmin(np.abs(self.electrolyzer_efficiency_rules[:, 0] - load))
        th_power = load * self.electrolyzer_efficiency_rules[closest_rule, 2] * self.max_el_power_uptake
        h2_power = load * self.electrolyzer_efficiency_rules[closest_rule, 1] * self.max_el_power_uptake
        h2_production = h2_power / self.h2.h_o

        return h2_production/2, h2_production, th_power

    def calculate_load(self, o2_demand: float):
        """
        Calculates the load of the electrolyzer based on the o2 demand.
        Arguments:
            o2_demand: float, o2 demand in Nm^3/h
        Returns:
            load: float, electricity load as a value between 0 and 1
        """
        # find the index of the load rule that is closest but greater or equal (geq) than the power demand
        # first, calculate the difference between the demand and O2-Production with all load rules
        o2_demand_with_rules = self.o2_production_w_loads - o2_demand
        # check_array is setting all negative elements to np.inf. This is necessary to find the index of the smallest element
        # if all elements are negative, the power demand is greater than the maximum power output of the boiler
        # in this case, the check_array contains only np.infs. further, load needs to be 1
        check_array = np.where(o2_demand_with_rules >= 0, o2_demand_with_rules, np.inf)
        if np.all(np.isinf(check_array)):
            load = 1.0
            return load
        else:
            closest_rule_idx = np.argmin(check_array) # returns the index of the closest load in self.electrolyzer_efficiency_rules
            # check if closest_rule_idx is in a stepless interval (that means, if closest load is geq than the lower bound and leq than the upper bound of any stepless interval).
            if np.any(np.logical_and(self.electrolyzer_stepless_intervals[:, 0] <= self.electrolyzer_efficiency_rules[closest_rule_idx, 0], self.electrolyzer_efficiency_rules[closest_rule_idx, 0] <= self.electrolyzer_stepless_intervals[:, 1])):
                # now calulate the load based on power needed and efficiency, but limit maximum load to 1
                load = min(o2_demand / (self.o2_production_w_efficiencies[closest_rule_idx]), 1)
            else:
                # if not, just take the closest load
                load = self.electrolyzer_efficiency_rules[closest_rule_idx, 0]
        return load
    
    def calculate_consumption(self, load: float) -> Tuple[float, float]:
        """
        Calculates the electricity and water consumption based on the load
        Arguments:
            load: float, electricity load as a value between 0 and 1
        Returns:
            el_consumption: float, electricity consumption in kW_el
            h2o_consumption: float, liquid water consumption in m^3/h
        """
        closest_rule = np.argmin(np.abs(self.electrolyzer_efficiency_rules[:, 0] - load))
        el_consumption = load * self.max_el_power_uptake
        # h2o_consumption uses the following formulas:
        # h2_production '/' h2_density_norm = h2_production_mass
        # h2_production_mass / h2_molar_weight * h2o_molar_weight = h2o_consumption_mass
        # h2o_consumption_mass / h2o_density = h2o_consumption_volume
        h2o_consumption = load * self.h2o_consumption_w_efficiencies[closest_rule]
        return el_consumption, h2o_consumption
    
    def capex(self) -> float:
        """
        Calculates the CAPEX of the electrolyzer
        """
        return self.capex_sp * self.max_el_power_uptake
    
    def opex(self):
        return self.opex_factor * self.capex()

# -------------


el_dict = {
    "eta_el_hu":    0.6742, # electrical efficiency of the electrolyzer in the hydrogen utilization process. Value between 0 and 1.
    "max_el_power_uptake":     2700, # electrical power consumption of the electrolyzer in kW
    "capex_sp":     1000, # specific CAPEX of the electrolyzer in EURO/kW_el
    "opex_factor":  0.05, # opex factor of the electrolyzer in EURO/kW_el/a
}

