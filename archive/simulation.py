# this will be the main script for the finalized project

from import_setup import *
from archive.electrolysis import *
from src.modules.methanation import *
from init import *
from params import *
from gases import *


# TODO: in the future, these variables can also be handled by a separate function/script
# for easier handling, different dataframes are created for the most needed quantities
# for type safety, the variables extracted from df are all specified as float arrays
# variables that are written into a new line every timestep
time_arr = df["Time / h"].values.astype(float)
time_diff = df["Time_diff / h"].values.astype(float)
raw_price = df["Price / EURO/MWh"].values.astype(float)
gas_production = df["Gas Production / Nm^3/h"].values.astype(float)
heat_demand_fermenter = df["Fermenter Heat Demand / kW"].values.astype(float)
chp_is_run = np.zeros((time_arr.shape[0], num_chps))
maintenance_time = np.zeros((time_arr.shape[0], num_chps, num_maintenance_rules))
chp_run_time = np.ones((time_arr.shape[0], num_chps))
maintenance = np.zeros((time_arr.shape[0], num_chps))
gas_storage = np.zeros(time_arr.shape)
chp_load = np.zeros((time_arr.shape[0], num_chps))
chp_el_power = np.zeros((time_arr.shape[0], num_chps))
chp_th_power = np.zeros((time_arr.shape[0], num_chps))
chp_gas_consumption = np.zeros((time_arr.shape[0], num_chps))
flare_run_time = np.zeros(time_arr.shape[0])
flare_gas_consumption = np.zeros(time_arr.shape[0])
temp_heat_net = np.zeros(time_arr.shape[0])
boiler_load = np.zeros((time_arr.shape[0], num_boilers))
boiler_th_power = np.zeros((time_arr.shape[0], num_boilers))
boiler_run_time = np.zeros((time_arr.shape[0], num_boilers))
boiler_gas_consumption = np.zeros((time_arr.shape[0], num_boilers))

# region production statistics initialization
production_statistics_template = {
    'O2 Demand' : 0,
    'O2 Storage': 0,
    'H2 Storage': 0
}
production_statistics = pd.DataFrame([production_statistics_template] * time_arr.shape[0])
production_statistics['O2 Demand'][0] = o2_sun['cons']
# endregion

# region storage policies initialization
storage_policies_template = {
    'O2 Minimum': 0
}
storage_policies = pd.DataFrame([storage_policies_template] * time_arr.shape[0])
storage_policies['O2 Minimum'][0] =  100  # [Nm^3] # [TODO] Example, replace with proper value
# endregion

# region production policies initialization
production_policies_template = {
    'Electrolyzer Opex Threshold': 0
}
production_policies = pd.DataFrame([production_policies_template] * time_arr.shape[0])
production_policies['Electrolyzer Opex Threshold'][0] =  10  # [EURO/Nm^3] # [TODO] Example, replace with proper value
# endregion

# region economic data initialization
economic_data_template = {
    'Electric Power': 0,
    'Water'         : 0
}
economic_data = pd.DataFrame([economic_data_template] * time_arr.shape[0])
economic_data['Electric Power'] = df["Price / EURO/MWh"]  # [EURO/MWh] # [TODO] Example, replace with proper value
economic_data['Water'][0] =  20             # [Nm^3] # [TODO] Example, replace with proper value
# endregion

# variables that are overwritten every timestep
run_time_between_maintenance = np.zeros([num_chps, chp_maintenance_rules.shape[1]], dtype=float)
maintenance_remaining = np.zeros(num_chps, dtype=float)



# initialize operator
from archive.load_scheduler import *
operator = LoadScheduler(flare_threshold, chp_gas_storage_rules, chp_maintenance_rules, chps, boilers, heat_net, price_threshold=80)
gas_storage[0] = 2402.79 # Nm^3
temp_heat_net[0] = 70 # °C


logging.debug("Starting simulation")
# Start the stopwatch
tic = time.time()

for k in range(1, len(time_arr)):
    # old_run_time=np.array([1,1]), old_maintenance=np.array([0,0]), old_gas_storage=1200, new_time_diff=1, new_price=0.1
    # run scheduler
    logging.debug("now calculating timestep: " + str(k))

    # calculate new temperature of heat network based on heat demand of fermenter
    temp_heat_net[k] = heat_net.new_temperature(heat_demand_fermenter[0],temp_heat_net[k-1],mass_flow_heat_net)
    # schedule runnning of chps and flare (and boilers)
    chp_run_time[k,:], maintenance[k,:], chp_load[k,:], maintenance_time[k,:], _, _, _,\
        run_time_between_maintenance, boiler_load[k,:], boiler_run_time[k,:], flare_run_time[k], flare_gas_consumption[k] = \
        operator.schedule_production(chp_run_time[k-1,:], maintenance[k-1,:],gas_storage[k-1], time_diff[k], raw_price[k], maintenance_remaining, flare_run_time[k-1], boiler_run_time[k-1,:], temp_heat_net[k], mass_flow_heat_net, 80)
    # run CHPs
    for i in range(num_chps):
        chp_load[k,i], chp_el_power[k,i], chp_th_power[k,i], chp_gas_consumption[k,i], maintenance_remaining[i], chp_run_time[k,:] = \
                chps[i].run(chp_load[k,i], run_time_between_maintenance[i,:], maintenance_remaining[i], chp_run_time[k,:], time_diff[k], biogas_obj.h_u)
    # run flare
    flare_gas_consumption[k] = flare.run(flare_gas_consumption[k])

    # run Boilers
    for i in range(num_boilers):
        boiler_load[k,i], boiler_th_power[k,i], boiler_gas_consumption[k,i], boiler_run_time[k,i] = \
            boilers[i].run(boiler_load[k,i], boiler_run_time[k,i], time_diff[k], biogas_obj.h_u)
    
    # calculate new temperature of heat network based on heat demand of fermenter
    temp_heat_net[k] = heat_net.new_temperature(np.sum(boiler_th_power[k,:])+np.sum(chp_th_power[k,:]),temp_heat_net[k-1],mass_flow_heat_net)

    # calculate new gas storage.
    # TODO: For now, the gas storage can be computed in main. If future complexity is coming in (if some costs are associated with it etc...) the gas storage needs to be separated into another object
    gas_storage[k] = gas_storage[k-1] + (gas_production[k] - chp_gas_consumption[k,:].sum() - flare_gas_consumption[k]) * time_diff[k] # in Nm^3

    # schedule and run electrolyzer
    scheduler_inputs = {
        'production_statistics': production_statistics.loc[k - 1],
        'storage_policies': storage_policies.loc[k - 1],
        'production_policies': production_policies.loc[k - 1],
        'economic_data': economic_data.loc[k - 1],
        'time_delta': time_diff[k - 1]
    }
    #print(f"before {production_statistics.loc[k - 1, 'O2 Storage']}")
    runtime, demand, storage = operator.schedule_electrolyzer(**scheduler_inputs)
    #print(f"after {production_statistics.loc[k - 1, 'O2 Storage']}")
    produced_O2, produced_H2 = electrolyzer.run(runtime, time_diff[k])
    # print(produced_O2)
    production_statistics.loc[k, 'O2 Storage'] = storage + produced_O2
    production_statistics.loc[k, 'H2 Storage'] = production_statistics.loc[k - 1, 'H2 Storage'] + produced_H2
    production_statistics.loc[k, 'O2 Demand'] = demand + (o2_sun['cons'] if time_arr[k] % 1.0 == 0 else 0)
    # every 1000 timesteps, print the current timestep as info log level
    if k % 10000 == 0:
        logging.info("timestep: " + str(k) + " of " + str(len(time_arr)))

    # For debugging, print a timestep near the problem to stop sim there
    if time_arr[k] == 7100*3:
        logging.debug("timestep: " + str(k) + " of " + str(len(time_arr)))

# Stop the stopwatch
toc = time.time()

logging.info('Simulation took {:3.2f}s to compute, with {} steps'.format(toc-tic, len(time_arr)))
