INTERVAL = 0.25 # (h)

# REVIEW: [subject to change or removal] just an idea
class GasParams:
	def __init__(self) -> None:
		self.O2 = {
			"rho_l":		24.0		# kg/m3
		}
		self.H2O = {
			"M":			0.018,		# kg/mol
			"rho_l":		998,		# kg/m³
			"cp_l":			4.18,		# kJ/kg/K
			"h_evap_5bar":	2104.09,	# kJ/kg
			"h_evap_10bar":	2014.9,		# kJ/kg
			"h_8bar":		2652.23,	# kJ/kg
			"kappa":		1.33,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
		}
		self.H2 = {
			"h_o":			3.542,		# kWh/Nm^3 upper (german: "oberer") heating value
			"h_u":			2.994,		# kWh/Nm^3 lower (german: "unterer") heating value
			"M":			0.002016,	# kg/mol
			"rho_norm":		0.08988,	# kg/Nm^3
			"kappa":		1.41,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
			#"v_dot":		o2_sun["cons"]*8760*2	# Nm^3/a minimum H2 production of electrolyzer **per year!** in order to meet the O2 demand
		}

		self.CH4 = {
			"h_o":			11.06,		# kWh/Nm^3
			"h_u":			9.97,		# kWh/Nm^3
			"rho_norm":		0.72,		# kg/Nm^3
			"M":			0.02,		# kg/mol
			"cp":			2.226,		# kJ/kg/K
			"kappa":		1.31,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
		}
  
		self.CO2 = {
			"rho_norm":		1.98,		# kg/Nm^3
			"cp":			0.846,		# kJ/kg/K
			"M":			0.0441,		# kg/mol
			"kappa":		1.30,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
		}

		self.N2 = {
			"M":			0.0147,		# kg/mol
			"rho_norm":		1.25,		# kg/Nm^3
			"cp":			1.04,		# kJ/kg/K
			"kappa":		1.40,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
		}
  
		self.SNG = {
			"ch4_frac":	0.95,
			"co2_frac":	0.01,
			"h2_frac":	0.04,
		}

		self.BIOGAS = { "ch4_frac": 0.6 } # fraction of CH4 in Biogas
		self.BIOGAS["co2_frac"] = 1 - self.BIOGAS["ch4_frac"]

class GlobalParams:
	def __init__(self):
		self.DATAFILE_PATH = "../data/data_w_benchmark_CHPs.csv"
		self.ELECTRICITY_DEMAND_PATH = "../data/data_w_benchmark_CHPs2.csv"
		self.FLARE_THRESHOLD = 1800
		self.MASS_FLOW_HEAT_NET = 180  # m^3/h
		
		self.PRICE_THRESHOLD = 80
		self.HEAT_NET_INIT_TEMP = 70 # °C
		
		self.BASE_ELECTRICITY_USAGE = 1000 # MWh/a
		self.O2_CONSUMPTION = 21.3637 # Nm^3/h -> 187.125 m^3/a in file

# region modules parameters 
class ChpParams:
	def __init__(self):
		self.STORAGE_RULES = {
			"chp1": {
				# name:   threshold, tendency, gas load
				# above 1200 Nm^3, tendency doesn't matter, full load
				"rule1": [1200, 0, 1],
				# above 800 Nm^3, tendency positive, 50% load
				"rule2": [800, 0, 0.54]
			},
			"chp2": {
				# name:   threshold, tendency, gas load
				# above 1200 Nm^3, tendency doesn't matter, full load
				"rule1": [1200, 0, 1],
				# above 800 Nm^3, tendency positive, 50% load
				"rule2": [800, 0, 0.54]
			},
			"chp3": {
				# name:   threshold, tendency, gas load
				# above 500 Nm^3, tendency doesn't matter, full load
				"rule1": [500, 0, 1],
				# above 800 Nm^3, tendency doesn't matter, 50% load
				"rule2": [400, 0, 0.55]
			},
			"chp4": {
				# name:   threshold, tendency, gas load
				# above 1000 Nm^3, tendency doesn't matter, full load
				"rule1": [1000, 0, 1],
				# above 800 Nm^3, tendency doesn't matter, 50% load
				"rule2": [800, 0, 0.55]
			}
		}

		self.EFFICIENCY_RULES = {
			"chp1": {
				# name: gas load, eta_el, eta_th
				"load0":    [0, 0, 0],
				"load0.54": [0.54, 0.373, 0.494],
				"load1":    [1, 0.408, 0.448]
			},
			"chp2": {
				# name: gas load, eta_el, eta_th
				"load0":    [0, 0, 0],
				"load0.54": [0.54, 0.373, 0.494],
				"load1":    [1, 0.408, 0.448]
			},
			"chp3": {
				# name: gas load, eta_el, eta_th
				"load0":    [0, 0, 0],
				"load0.55": [0.55, 0.373, 0.494],
				"load1":    [1, 0.408, 0.448]
			},
			"chp4": {
				# name: gas load, eta_el, eta_th
				"load0":    [0, 0, 0],
				"load0.55": [0.55, 0.373, 0.494],
				"load1":    [1, 0.408, 0.448]
			}
		}

		self.MAINTENANCE_RULES = {

			"chp1": {
				# name: interval, duration
				# every 1000 hours, maintenance duration 5 hours
				"rule1": [1026, 25],
				# one more rule for maintenance duration 5 hours
				"rule2": [100000, 5],
				# just another rule to show the capability of the LoadScheduler
				"rule3": [100000, 5],
			},
			"chp2": {
				"rule1": [1052, 14],
			},
			"chp3": {
				"rule1": [2786, 16],
			},
			"chp4": {
				"rule1": [1048, 18],
			}
		}

		self.MAX_POWER = {
			"chp1": 2086,  # kW
			"chp2": 2086,  # kW
			"chp3": 1302,  # kW
			"chp4": 1302,  # kW
		}

		self.FAILURE_RULES = {
			# name: avg occurence, duration
			# fails in average every 144 hours, duration of failure/maintenance 12 hours
			"chp1": [144, 12],
			# fails in average every 124 hours, duration of failure/maintenance 16 hours
			"chp2": [124, 16],
			# fails in average every 171 hours, duration of failure/maintenance 10 hours
			"chp3": [171, 10],
			# fails in average every 98 hours, duration of failure/maintenance 35 hours
			"chp4": [98, 35],
		}
		self.CAPEX_SP = {
			"chp1": 411,  # €/kW
			"chp2": 411,  # €/kW
			"chp3": 411,  # €/kW
			"chp4": 411,  # €/kW
		}

		self.CAPEX = {
			"chp1": self.CAPEX_SP["chp1"]* self.MAX_POWER["chp1"],  # €
			"chp2": self.CAPEX_SP["chp2"]* self.MAX_POWER["chp2"],  # €
			"chp3": self.CAPEX_SP["chp3"]* self.MAX_POWER["chp3"],  # €
			"chp4": self.CAPEX_SP["chp4"]* self.MAX_POWER["chp4"],  # €

		}

		self.NAMES = list(self.STORAGE_RULES.keys())
		self.COUNT = len(self.STORAGE_RULES.keys())

class BoilerParams:
	def __init__(self):
		self.MAX_POWER = {
			"boiler1": 2448,  # kW of gas
			"boiler2": 2448  # kW of gas
		}

		self.EFFICIENCY_RULES = {
			"boiler1": {
				# name: gas load, eta_th
				"load0":    [0, 0],
				"load0.3":  [0.3, 0.9],
				"load1":    [1, 0.9]
			},
			"boiler2": {
				# name: gas load, eta_th
				"load0":    [0, 0],
				"load0.3":  [0.3, 0.9],
				"load1":    [1, 0.9]
			}
		}

		self.STEPLESS_INTERVALS = {
			"boiler1": {
				# intervals for stepless control of boilers
				# name, lower bound, upper bound
				"0.3-1":    [0.3, 1]
			},
			"boiler2": {
				# name, lower bound, upper bound
				"0.3-1":    [0.3, 1]
			}
		}
		self.CAPEX_SP = {
			"boiler1": 68,  # €/kW
			"boiler2": 68,  # €/kW
		}

		self.CAPEX = {
			"boiler1": self.CAPEX_SP["boiler1"]* self.MAX_POWER["boiler1"],  # €
			"boiler2": self.CAPEX_SP["boiler2"]* self.MAX_POWER["boiler2"],  # €
		}

		self.COUNT = len(self.MAX_POWER.keys())
		self.NAMES = list(self.MAX_POWER.keys())

class FlareParams:
	def __init__(self) -> None:
		self.MAX_GAS = {
			"flare" : 1000,  # Nm^3/h
		}
		self.CAPEX = {
			"flare": 0,  # €
		}
		self.COUNT = len(self.MAX_GAS.keys())
		self.NAMES = list(self.MAX_GAS.keys())

class CoolerParams:
	def __init__(self) -> None:
		self.MAX_HEAT = {
			"cooler" : 1000000000,  # kW
		}
		self.CAPEX = {
			"cooler": 0,  # €
		}
		self.COUNT = len(self.MAX_HEAT.keys())
		self.NAMES = list(self.MAX_HEAT.keys())

class ElectrolyzerParams:
	def __init__(self) -> None:
		self.EFFICIENCY_RULES = {
			"electrolyzer1": {
				# name: el_load, eta_ho, eta_th (here, it is just the surplus heat from electrolysis)
				"load0":    [0, 0.7969, 0.2031],
				"load1":    [1, 0.7969, 0.2031]
			},
		}

		self.STEPLESS_INTERVALS = {
			"electrolyzer1": {
				# intervals for stepless control of electrolyzer
				# name, lower bound, upper bound
				"0-1":    [0, 1]
			},
		}

		self.MAX_POWER = {
			"electrolyzer1": 2700,  # kW of electricity
		}

		self.CAPEX_SP = {
			"electrolyzer1": 1000, # Euro/KW
		}

		self.CAPEX = {
			"electrolyzer1": self.CAPEX_SP["electrolyzer1"] * self.MAX_POWER["electrolyzer1"] , # Euro
		}

		self.COUNT = len(self.MAX_POWER.keys())
		self.NAMES = list(self.MAX_POWER.keys())

class MethanationParams:
	def __init__(self) -> None:
		self.EFFICIENCY_RULES = {
			"meth1": {
				# name: gas load, yield_ch4, eta_th (here, it is just the surplus heat from methanation, with 20% losses.)
				"load0":    [0, 0.875, 0.0965],
				"load0.5":  [0.5, 0.875, 0.0965],
				"load1":    [1, 0.875, 0.0965],
			},
		}
		# eta_th is defined as gas load [kW] / theoretical heat output [kW] * efficiency = 2461 / 297 * 0.8 = 0.0965
		# heat output is calculated by \Delta H_{meth} * n^._{ch4} = 297 kW

		self.STEPLESS_INTERVALS = {
			"meth1": {
				# intervals for stepless control of electrolyzer
				# name, lower bound, upper bound
				"0-0":    [0, 0]
			},
		}

		self.MAX_POWER = {
			"meth1": 2461,  # kW of gas. Reactor is normed to take 60/40 of CH4/CO2. Corresponds to upper heating value of 371 Nm^3/h biogas of 60/40 CH4/CO2

		}
		self.CAPEX_SP = 500 # Euro/KW
		
		self.CAPEX = {
			"meth1": self.CAPEX_SP * self.MAX_POWER["meth1"] # €
		}

		self.COUNT = len(self.MAX_POWER.keys())
		self.NAMES = list(self.MAX_POWER.keys())
# endregion modules parameters

GAS_PARAMS = GasParams()
GLOBAL_PARAMS = GlobalParams()
CHP_PARAMS = ChpParams()
BOILER_PARAMS = BoilerParams()
ELECTROLYZER_PARAMS = ElectrolyzerParams()
METHANATION_PARAMS = MethanationParams()
FLARE_PARAMS = FlareParams()
COOLER_PARAMS = CoolerParams()