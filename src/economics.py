from config import GLOBAL_PARAMS as PARAMS, GAS_PARAMS
import pandas as pd
from config import GLOBAL_PARAMS
from helpers import import_historic_prices, calculate_real_price

THERMAL_PRICE = 0.075 # €/kWh
SNG_PRICE = 0.075 # €/kWh
WATER_PRICE = 4 # €/Nm³
BIOGAS_PRICE = 0.014 # €/KWh
HYDROGEN_PRICE = 0.3 # €/Nm³
OXYGEN_PRICE = 0.18 # €/kg
FERMENTER_ELECTRICITY_PROCUREMENT = 229.0 # €/MWh
ELECTRICITY_NETWORK = 1.2 # €/MWh


EEG = 37.23 # €/MWh
eeg_surcharge_factor = 0.2
EEG_SURPLUS = EEG * eeg_surcharge_factor # €/MWh

KWKG = 3.78 # €/MWh
kwkg_surcharge_factor = 0.2
KWKG_SURPLUS = KWKG * kwkg_surcharge_factor # €/MWh

STROM_NEV = 4.37
STROM_NEV_SURPLUS = 0.5

ENWG = 4.19
enwg_surcharge_factor = 0.2
ENWG_SURPLUS = ENWG * enwg_surcharge_factor
ABLAV = 0.03
CONCESSION_FEE = 1.10 # €/MWh
CONCESSION_REFUND = 1.10 # €/MWh

ELECTRICITY_TAX = 20.5
ELECTRICITY_TAX_REFUND = 5.13

TAX_RATE = 0.25
DEBT_RATIO = 1
BANK_INTEREST_RATE = 0.05
PAYBACK_TIME = 10
MAINTENANCE_COST = 0.01 # % of investment costs
INSURANCE_COST = 0.01 # % of investment costs

def import_electricity_prices_from_csv():
	df: pd.DataFrame = import_historic_prices(GLOBAL_PARAMS.DATAFILE_PATH)
	df["Real Price / EURO/MWh"] = calculate_real_price(df["Price / EURO/MWh"].values)
	raw_price = df["Price / EURO/MWh"].values.astype(float)
	return raw_price

class Economics:
	def __init__(self) -> None:
		self.electricity_prices = import_electricity_prices_from_csv()
		self.__time = 0

	def get_incomes(self, **prices):
		thermal_energy_price = prices['thermal_energy_price']
		electricity_price = prices['electricity_price']
		sng_price = prices['SNG_price']
		chps_revenue = 0
		# for chp in self.chps:
		# 	chps_revenue += chp.Products[0] * electricity_price + chp.Products[1] * thermal_energy_price
		# boilers_revenue = 0
		# for boiler in self.boilers:
		# 	boilers_revenue += boiler.Products[0] * thermal_energy_price
		# methanation_revenue = 0
		# methanation_revenue += methanation.Products[0] * utility_cost_price[1]
		total_revenue = self.sng_production * sng_price + self.net_electricity * electricity_price + self.net_heat * thermal_energy_price
		return  total_revenue
		#methanation_revenue += self.methanation.Products[0] * self.SNGPricePerKW

	def __calculate_utility_costs(self, **prices):
		electricity_price = prices['electricity_price']
		water_price = prices['water_price']
		# electrolyzer_cost = 0
		# electrolyzer_cost += self.electrolyzer.Consumption[0] * electricity_price + self.electrolyzer.Consumption[1] * water_price
		total_utility_cost = self.net_electricity * electricity_price + self.net_water * water_price
		return total_utility_cost

	def __calculate_electricity_purchase_cost(self, amount_purchased):
		
		purchase_cost = amount_purchased * self.electricity_prices[self.__time]
  
		production_costs = amount_purchased * FERMENTER_ELECTRICITY_PROCUREMENT
		network_cost = amount_purchased * ELECTRICITY_NETWORK
		surcharges = 0
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * EEG
		surcharges += (amount_purchased - PARAMS.BASE_ELECTRICITY_USAGE) * EEG_SURPLUS
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * KWKG
		surcharges += (amount_purchased - PARAMS.BASE_ELECTRICITY_USAGE) * KWKG_SURPLUS
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * STROM_NEV
		surcharges += (amount_purchased - PARAMS.BASE_ELECTRICITY_USAGE) * STROM_NEV_SURPLUS
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * ENWG
		surcharges += (amount_purchased - PARAMS.BASE_ELECTRICITY_USAGE) * ENWG_SURPLUS
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * ABLAV
		surcharges += PARAMS.BASE_ELECTRICITY_USAGE * CONCESSION_FEE
		surcharges -= PARAMS.BASE_ELECTRICITY_USAGE * CONCESSION_REFUND
		tax = 0
		tax += amount_purchased * ELECTRICITY_TAX
		surcharges -= amount_purchased * ELECTRICITY_TAX_REFUND + 250

		return purchase_cost # + production_costs + network_cost + surcharges #REVIEW - is this true?

	def __calculate_electricity_production_income(self, amount_produced):
		return amount_produced * self.electricity_prices[self.__time]

	def __calculate_oxygen_purchase_cost(self, amount_purchased):
		'''
		purchased_oxygen: m3
		'''
		return amount_purchased * OXYGEN_PRICE * GAS_PARAMS.O2["rho_l"]

	def	__calculate_oxygen_production_income(self, amount_produced):
		return 0

	def __get_debt_payment(self):
		chps_annual_debt_payment = 0
		for chp in self.chps:
			chps_annual_debt_payment += chp.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))

		boilers_annual_debt_payment = 0
		for boiler in self.boilers:
			boilers_annual_debt_payment += boiler.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))
		electrolyzer_annual_debt_payment = self.electrolyzer.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))
		methanation_annual_debt_payment = self.methanation.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))
		flare_annual_debt_payment = self.flare.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))
		fermenter_annual_debt_payment = self.fermenter.capex * DEBT_RATIO * ( BANK_INTEREST_RATE )* 1/(1-1/(1+BANK_INTEREST_RATE)**(PAYBACK_TIME))
		total = chps_annual_debt_payment + boilers_annual_debt_payment + electrolyzer_annual_debt_payment + methanation_annual_debt_payment + flare_annual_debt_payment + fermenter_annual_debt_payment
		return (total / 365 * 24 * 4)

	def __get_maintenance_costs(self):
		return self.get_debt_payment() * MAINTENANCE_COST

	def __get_insurance_costs(self):
		return self.get_debt_payment() * INSURANCE_COST

	def __get_total_cost(self, **prices):
		return self.get_utility_costs(**prices) + self.get_debt_payment() + self.get_maintenance_costs() + self.get_insurance_costs()

	def calculate_economic_stats(self, net_electricity, net_oxygen, time):
		self.__time = time
		gross_cash_flow: float = 0.0
		net_cash_flow: float = 0.0
		costs: float = 0.0
		incomes: float = 0.0
		if net_electricity > 0:
			costs += self.__calculate_electricity_purchase_cost(net_electricity)
			pass
		else:
			incomes += self.__calculate_electricity_production_income(-1 * net_electricity)
			pass
   
		if net_oxygen > 0:
			costs += self.__calculate_oxygen_purchase_cost(net_oxygen)
			pass
		else:
			incomes += self.__calculate_oxygen_production_income(-1 * net_oxygen)
   
		gross_cash_flow = incomes + costs
		net_cash_flow =  (incomes - costs) * (1 - TAX_RATE)
		net_present_value = 0.0 #FIXME - net_cash_flow / (1 + GLOBAL_PARAMS.BANK_INTEREST_RATE) ** (time_arr[k] / (365 * 24))

		return gross_cash_flow, net_cash_flow, net_present_value