from numba import njit, typeof
from numba.experimental import jitclass
from numba import int32, int64, float32, float64, boolean, bool_, void, optional, types
from numba.typed import List

import logging, sys
from config import GAS_PARAMS

# A library with gas properties.
# TODO: This should be extended to a more object-oriented approach.

@jitclass([('h_o', float64), ('h_u', float64), ('M', float64), ('kappa', float64), ('mass', float64), ('rho_norm', float64), ('cp', float64), ('h_evap_5bar', float64), ('h_evap_10bar', float64), ('h_8bar', float64), ('rho_l', float64), ('cp_l', float64)])
class Gas():
    """
    A class that represents a gas species. Contains properties such as density, enthalpy, etc.
    """
    def __init__(self, M:float, h_o:float=0, h_u:float=0, kappa:float=0, mass:float=0, rho_norm:float=0, cp:float=0, h_evap_5bar:float=0, h_evap_10bar:float=0, h_8bar:float=0, rho_l:float=0, cp_l:float=0):
        self.h_o = h_o
        self.h_u = h_u
        self.M = M
        self.kappa = kappa
        self.mass = mass
        self.rho_norm = rho_norm
        self.cp = cp
        self.h_evap_5bar = h_evap_5bar
        self.h_evap_10bar = h_evap_10bar
        self.h_8bar = h_8bar
        self.rho_l = rho_l
        self.cp_l = cp_l


h2 = Gas(**GAS_PARAMS.H2)
ch4 = Gas(**GAS_PARAMS.CH4)
h2o = Gas(**GAS_PARAMS.H2O)
co2 = Gas(**GAS_PARAMS.CO2)
n2 = Gas(**GAS_PARAMS.N2)

@jitclass([('ch4_frac', float64), ('co2_frac', float64), ('h2_frac', float64), ('h2o_frac', float64), ('n2_frac', float64), ('h2', typeof(h2)), ('ch4', typeof(ch4)), ('h2o', typeof(h2o)), ('co2', typeof(co2)), ('n2', typeof(n2)), ('h_o', float64), ('h_u', float64), ('M', float64), ('rho_norm', float64), ('cp', float64), ('kappa', float64)])
class GasMix():
    def __init__(self, ch4_frac:float=0, co2_frac:float=0, h2_frac:float=0, h2o_frac:float=0, n2_frac:float=0, h2:Gas=h2, ch4:Gas=ch4, h2o:Gas=h2o, co2:Gas=co2, n2:Gas=n2):
        """
        A class that represents a Gas mixture of CH4, CO2, H2, H2O and N2.
        Contains composition as well as deducted properties (heating value etc.)
        """
        self.ch4_frac = ch4_frac
        self.co2_frac = co2_frac
        self.h2_frac = h2_frac
        self.h2o_frac = h2o_frac
        self.n2_frac = n2_frac

        total_frac = self.ch4_frac + self.co2_frac + self.h2_frac + self.h2o_frac + self.n2_frac

        self.ch4 = ch4
        self.co2 = co2
        self.h2 = h2
        self.h2o = h2o
        self.n2 = n2

        self.h_o = (ch4.h_o * self.ch4_frac + co2.h_o * self.co2_frac + h2.h_o * self.h2_frac + h2o.h_o * self.h2o_frac + n2.h_o * self.n2_frac) / total_frac
        self.h_u = (ch4.h_u * self.ch4_frac + co2.h_u * self.co2_frac + h2.h_u * self.h2_frac + h2o.h_u * self.h2o_frac + n2.h_u * self.n2_frac) / total_frac
        self.kappa = (ch4.kappa * self.ch4_frac + co2.kappa * self.co2_frac + h2.kappa * self.h2_frac + h2o.kappa * self.h2o_frac + n2.kappa * self.n2_frac) / total_frac
        self.rho_norm = (ch4.rho_norm * self.ch4_frac + co2.rho_norm * self.co2_frac + h2.rho_norm * self.h2_frac + h2o.rho_norm * self.h2o_frac + n2.rho_norm * self.n2_frac) / total_frac

    def __setattr__(self, name: str, value: float) -> None:
        # if a composition is changed, the heating value as well as the total fraction is recalculated
        if "_frac" in name:
            self.__dict__[name] = value
            # if the total fraction is changed, the other fractions are adjusted
            # it throws an error during initialization, therefore the try-except
            try:
                self.total_frac = self.ch4_frac + self.co2_frac + self.h2_frac + self.h2o_frac + self.n2_frac
                self.h_o = (self.ch4.h_o * self.ch4_frac + self.co2.h_o * self.co2_frac + self.h2.h_o * self.h2_frac + self.h2o.h_o * self.h2o_frac + self.n2.h_o * self.n2_frac) / self.total_frac
                self.h_u = (self.ch4.h_u * self.ch4_frac + self.co2.h_u * self.co2_frac + self.h2.h_u * self.h2_frac + self.h2o.h_u * self.h2o_frac + self.n2.h_u * self.n2_frac) / self.total_frac
                self.kappa = (self.ch4.kappa * self.ch4_frac + self.co2.kappa * self.co2_frac + self.h2.kappa * self.h2_frac + self.h2o.kappa * self.h2o_frac + self.n2.kappa * self.n2_frac) / self.total_frac
                self.rho_norm = (self.ch4.rho_norm * self.ch4_frac + self.co2.rho_norm * self.co2_frac + self.h2.rho_norm * self.h2_frac + self.h2o.rho_norm * self.h2o_frac + self.n2.rho_norm * self.n2_frac) / self.total_frac
            except:
                pass
        else:
            self.__dict__[name] = value
            
    def __repr__(self) -> str:
        return f"GasMix(ch4_frac={self.ch4_frac}, co2_frac={self.co2_frac}, h2_frac={self.h2_frac}, h2o_frac={self.h2o_frac}, n2_frac={self.n2_frac}, h_o={self.h_o}, h_u={self.h_u}, kappa={self.kappa}, rho_norm={self.rho_norm})"

biogas = GasMix(**GAS_PARAMS.BIOGAS)
sng = GasMix(**GAS_PARAMS.SNG)


logging.debug("Imported gas properties.")