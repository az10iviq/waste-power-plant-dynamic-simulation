import pandas as pd
import numpy as np
import logging
import pint
import os

# TODO: @Fatemeh, if you don't need this Pint library anymore, you can remove it :)
# I think pint_pandas uses its own registry.
# So we need to get the already set-up registry instead of creating a new one
# in order to have consistency with the custom Quantities
si = pint.get_application_registry()
si.define('euro = [currenta] = EUR')
si.define('cent = euro/100 = Ect')

# specific units needed for energy process engineering
si.define('norm_cubic_meter = meter**3 = ncm')
si.define('watt_electric = watt = W_el')
si.define('watt_thermic = watt = W_th')

# define fraction from [here](https://stackoverflow.com/questions/39153885/how-to-define-and-use-percentage-in-pint)
si.define('fraction = [] = frac')
si.define('percent = 1e-2 frac = pct')
si.define('ppm = 1e-6 fraction')


def import_historic_prices(csvfile: str): #, df: pd.DataFrame) # for future implementation
        """
        TODO: This function should be split up into the following functions:
        - import the time data (should be implemented in a separate object) and calculate the time difference Time_diff. The return is the original dataframe df, which is then used in the following functions. This object can also hold a method to merge DataFrames, as in merge_csv_files.py
        - import the historic prices to the original dataframe df (should be implemented here)
        Also, add better docstring :)
        """
        ## This is the part of the function that should be implemented in a separate object
        # Import historic electricity prices from power-pricess_2018.csv into a pandas dataframe
        file_path =  os.path.dirname(__file__) + "/" + csvfile
        df = pd.read_csv(file_path, delimiter=",", decimal=".")
        df["Time"] = pd.to_datetime(df["Time"], format="%Y-%m-%d %H:%M:%S") # format input Timestamps so pandas can understand it
        # getting the time difference from Timestamps. Adapted from [here](https://stackoverflow.com/questions/49853295/calculate-cumulative-duration-of-a-pandas-datetime-column)

        df["Time / h"] = pd.to_timedelta(df["Time"]-df["Time"][0]) / pd.to_timedelta(1, unit='H') + 0.25
        df["Time / h"] = pd.Series(df["Time / h"], dtype="float64")

        # define the time difference between two consecutive Timestamps. It's important for calculations regarding run_time etc.
        # TODO: Make sure the time_diff[k] is the time difference between Timestamp[k+1] and Timestamp[k]
        df["Time_diff / h"] = df["Time / h"].diff().fillna(df["Time / h"][0])

        df["Price / EURO/MWh"] = pd.Series(df["Day-Ahead-Price [EURO/MWh]"], dtype="float64")

        # drop the old columns
        df = df.drop(columns=["Day-Ahead-Price [EURO/MWh]"])


        ## This is the part of the function that should be implemented here
        # Import historic electricity prices from pcsvfile into a pandas dataframe
        # TODO: Lots of this code is redundant to import_data in gas_production.py (especially the import and integrity checks). Could be implemented as a mother class with inheritance to ElectricityGrid and Fermenter?
        file_path =  os.path.dirname(__file__) + "/" + csvfile
        df_ep = pd.read_csv(csvfile, delimiter=",", decimal=".")
        df_ep["Time"] = pd.to_datetime(df_ep["Time"], format="%Y-%m-%d %H:%M:%S") # format input Timestamps so pandas can understand it
        df_ep["Time / h"] = pd.to_timedelta(df_ep["Time"]-df_ep["Time"][0]) / pd.to_timedelta(1, unit='H') + 0.25

        # Check if df_ep["Time / h"] is equal to df["Time / h"]. If not, raise an error
        # TODO: if the Time is not equal, interpolate the missing values or average them so that df_ep fits df as in merge_csv_files (in import_setup.py)
        if not df_ep["Time / h"].equals(df["Time / h"]):
            raise ValueError("The time steps of the historic electricity prices and the simulation do not match. Please check the input files.")
        
        # Check if df["Price / EURO/MWh"] is already filled. If yes, print a warning and return the original dataframe. If not, fill the column with the historic prices
        if not df["Price / EURO/MWh"].isnull().values.any():
            logging.debug("The column 'Price / EURO/MWh' is already filled. I will skip the import of historic prices.")
            return df
        else:
            df["Price / EURO/MWh"] = df_ep["Price / EURO/MWh"]
            return df

def calculate_real_price(raw_price) -> pd.DataFrame:
        """
        This function calculates the real electricity price by adding the costs and taxes.
        Input:
            raw_price: The raw electricity price without taxes and costs. Must be given as np.array or float.
        Output:
            real_price: The real electricity price in EURO/MWh
        TODO: This function can be made more interactive in the future (e.g. passing the values via a dict from main or similar). Thus, it gets easier to play with scenarios (e.g. changing taxes etc.)
        """
        # great source: composition of electricity prices for German industry: [Link](https://www.bdew.de/media/documents/220124_BDEW-Strompreisanalyse_Januar_2022_24.01.2022_final.pdf)
        # found it from [statista](https://de.statista.com/statistik/daten/studie/168571/umfrage/strompreis-fuer-die-industrie-in-deutschland-seit-1998/)

        # For KWK-, Offshore, StromNEV-Umlage: The first 1 GWh normally have to be paid normal fee. But with estimated 20 GWh_el/a, that is pretty little. Thus, it was neglected.

        # EEG-Umlage is very little for electrochemical production of H2 (15% of normal EEG-Umlage). Applies regardless of power source (conventional/renewable). Can be further reduced up to 0.1 ct/kWh. A little more complex (has "cap" and "super-cap")
        eeg = si.Quantity(6.5, "Ect/kWh") * 0.15 # 6.5 ct/kWh is average EEG-Umlage for households in 2021 (based on BDEW, see above)

        # normal power tax: 2.05 ct/kWh as from [here](https://www.bundesfinanzministerium.de/Content/DE/Glossareintraege/S/Stromsteuer.html?view=renderHelp)
        # reduced power tax: 1.573 ct/kWh for all industry
        # no power tax: for electrolysis as of [§9 StromStG](https://www.gesetze-im-internet.de/stromstg/__9a.html)
        power_tax = si.Quantity(0, "Ect/kWh")

        # konzessionsabgabe for consumers > 30000  kWh/a and above 30 kW: 0.11 ct/kWh from [here](https://www.acteno.de/ecms/de/energieanwendungen-4-0/konzessionsabgabe.html)
        # could also be 0 ct/kWh if Abnahmepreis < Grenzpreis. Grenzpreis is published yearly by Statistisches Bundesamt (Source: BDEW, see above)
        konzession = si.Quantity(0.11, "Ect/kWh")

        # KWK-Umlage is (pretty similar to EEG-Umlage): 15% of normal KWK-Umlage, minimum 0.03 ct/kWh. A little more complex (has "cap" and "super-cap")
        kwk = si.Quantity(0.254, "Ect/kWh") * 0.15 # 0.254 ct/kWh is average Umlage for households in 2021 (based on BDEW, see above)

        # Offshore-Umlage is (pretty similar to EEG-Umlage): 15% of normal Offshore-Umlage, minimum 0.03 ct/kWh. A little more complex (has "cap" and "super-cap")
        offshore = si.Quantity(0.395, "Ect/kWh") * 0.15 # 0.395 ct/kWh is average Umlage for households in 2021 (based on BDEW, see above)

        # For producing industry, the strom_NEV-fee is very low.
        strom_NEV = si.Quantity(0.025, "Ect/kWh") # Year: 2022! From [here](https://www.netztransparenz.de/EnWG/-19-StromNEV-Umlage/-19-StromNEV-Umlagen-Uebersicht/-19-StromNEV-Umlage-2022)

        # netzentgelte are normally published by the Netzbetreiber online.
        # it is reduced to:
        # 20%, if usage <7k h/a
        # 15%, if usage <7.5k h/a
        # 10%, if usage <8k h/a
        netzentgelt = si.Quantity(7.8, "Ect/kWh") * 0.2 # 7.8 ct/kWh is average Netzentgelt for households in 2021 (based on BDEW, see above). Pessimistic estimation for industry.

        abschaltbare_lasten = si.Quantity(0.009, "Ect/kWh") # from [netztransparenz](https://www.netztransparenz.de/EnWG/Abschaltbare-Lasten-Umlage/Abschaltbare-Lasten-Umlagen-Uebersicht/AbLaV-Umlage-2021)

        vat = 1.19 # German MWSt., multiplies with the sum of the other fees
        
        price = ((si.Quantity(raw_price, "EUR/MWh") + netzentgelt) + eeg + power_tax + offshore + kwk + strom_NEV + konzession + abschaltbare_lasten) * vat
        
        return price.magnitude

# function that takes chp_maintenance_rules or _storage and returns a pure numpy array (not containing any lists) with the maintenance schedule (or gas storage rules respectively) as well as the number of rules for each CHP
# it uses the max(map(len, list(chp_maintenance_rules.values()))) function to determine the number of rules for each CHP
# and np.array([xi+[None]*(length-len(xi)) for xi in x]) to fill the array with None values
def get_rules(chp_rules):
    """
    TODO: add description
    nice description should be here, but isnt because of lack of time
    """
    # check if all rule-tables are dicts
    if not isinstance(chp_rules, dict):
        raise TypeError("chp_rules must be a dict")
            # some checks on data integrity
            # check if values in chp_maintenance_rules subdicts are lists
    for i in list(chp_rules.keys()):
        for j in list(chp_rules[i].keys()):
            if not isinstance(chp_rules[i][j], list):
                raise TypeError(f"chp_rules[{i}] must be a dict of lists")

    # get the number of rules for each CHP
    # map applies the len function to each element of the list chp_rules.values()
    n_rules = max(map(len, list(chp_rules.values())))
    # get maximum length of rules
    max_rule_length = max(map(len, [list(chp_rules[i].values()) for i in list(chp_rules.keys())][0]))
    # create an empty array with the correct dimensions
    rules = np.empty((len(chp_rules), n_rules, max_rule_length), dtype=object)
    # fill the array with the rules
    for i, chp in enumerate(chp_rules):
        for j, rule in enumerate(chp_rules[chp]):
            for k in range(0, max_rule_length):
                try:
                    rules[i, j, k] = chp_rules[chp][rule][k]
                except IndexError:
                    rules[i, j, k] = None
    # convert rules array to a float array
    rules = rules.astype(float)
    n_chps = len(chp_rules.keys())
    return rules, n_rules, n_chps

def import_data(df: pd.DataFrame, csvfile: str, target_col: str) -> pd.DataFrame:
        """
        Import historic data from csv file into a pandas dataframe analogue to import_historic_prices in el_grid.py
        Input:
            csvfile: The path to the csv file with the historic Data. The file needs columns with name "Time" (formatted as "%Y-%m-%d %H:%M:%S") and target_col
            df: The dataframe to which the historic Data is added. Needs column with name "Time / h"
            target_col: The name of the column in which the historic Data is added
        Output:
            df: The pandas dataframe with the historic Data
        """
        # Import historic Data from csv file
        # For further information of the procedure, look at the comments in el_grid.py
        file_path =  os.path.dirname(__file__) + "/" + csvfile
        df_gp = pd.read_csv(file_path, delimiter=",", decimal=".")
        df_gp["Time"] = pd.to_datetime(df_gp["Time"], format="%Y-%m-%d %H:%M:%S") # format input Timestamps so pandas can understand it
        df_gp["Time / h"] = pd.to_timedelta(df_gp["Time"]-df_gp["Time"][0]) / pd.to_timedelta(1.0, unit='hr') + 0.25


        if not df_gp["Time / h"].equals(df["Time / h"]):
            raise ValueError("The time steps of the new Data and the simulation do not match. Please check the input files.")
        
        # Check if df[target_col] exists. If yes, print a warning and return the original dataframe (skip). If not, fill the column with the historic Data. 
        if target_col in df.columns:
            logging.debug(f"The column {target_col} is already filled. I will skip the import.")
            return df
        else:
            df[target_col] = df_gp[target_col]
            return df