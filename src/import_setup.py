import numpy as np
import pint
import pandas as pd
# import matplotlib.pyplot as plt
# import ipympl
import time
import warnings
import random
from numba import njit, typeof
from numba.experimental import jitclass
from numba import int32, int64, float32, float64, boolean, bool_, void, optional, types
from numba.typed import List
import logging, sys
from typing import Tuple
from scipy.optimize import dual_annealing
import os


# If you want to see the debug logs, just set level=logging.DEBUG
# A quick introduction to logging: https://realpython.com/python-logging/
logging.basicConfig(stream=sys.stderr, level=logging.INFO)

# if you want to see debug logs in numba classes, just set the environment variable NUMBA_DEBUGINFO = "1"
os.environ["NUMBA_DEBUGINFO"] = "0"

# TODO: @Fatemeh, if you don't need this Pint library anymore, you can remove it :)
# I think pint_pandas uses its own registry.
# So we need to get the already set-up registry instead of creating a new one
# in order to have consistency with the custom Quantities
si = pint.get_application_registry()

# define currency to calculate economic quantities
si.define('euro = [currenta] = EUR')
si.define('cent = euro/100 = Ect')

# specific units needed for energy process engineering
si.define('norm_cubic_meter = meter**3 = ncm')
si.define('watt_electric = watt = W_el')
si.define('watt_thermic = watt = W_th')

# define fraction from [here](https://stackoverflow.com/questions/39153885/how-to-define-and-use-percentage-in-pint)
si.define('fraction = [] = frac')
si.define('percent = 1e-2 frac = pct')
si.define('ppm = 1e-6 fraction')

# this is a debugging message, which only appears at level=logging.DEBUG in logging.basicConfig
logging.debug("Imported all modules and set up SI unit registry.")




# here are some helper functions that might be needed elsewhere

# function that takes chp_maintenance_rules or _storage and returns a pure numpy array (not containing any lists) with the maintenance schedule (or gas storage rules respectively) as well as the number of rules for each CHP
# it uses the max(map(len, list(chp_maintenance_rules.values()))) function to determine the number of rules for each CHP
# and np.array([xi+[None]*(length-len(xi)) for xi in x]) to fill the array with None values
def get_rules(chp_rules):
    """
    TODO: add description
    nice description should be here, but isnt because of lack of time
    """
    # check if all rule-tables are dicts
    if not isinstance(chp_rules, dict):
        raise TypeError("chp_rules must be a dict")
            # some checks on data integrity
            # check if values in chp_maintenance_rules subdicts are lists
    for i in list(chp_rules.keys()):
        for j in list(chp_rules[i].keys()):
            if not isinstance(chp_rules[i][j], list):
                raise TypeError(f"chp_rules[{i}] must be a dict of lists")

    # get the number of rules for each CHP
    # map applies the len function to each element of the list chp_rules.values()
    n_rules = max(map(len, list(chp_rules.values())))
    # get maximum length of rules
    max_rule_length = max(map(len, [list(chp_rules[i].values()) for i in list(chp_rules.keys())][0]))
    # create an empty array with the correct dimensions
    rules = np.empty((len(chp_rules), n_rules, max_rule_length), dtype=object)
    # fill the array with the rules
    for i, chp in enumerate(chp_rules):
        for j, rule in enumerate(chp_rules[chp]):
            for k in range(0, max_rule_length):
                try:
                    rules[i, j, k] = chp_rules[chp][rule][k]
                except IndexError:
                    rules[i, j, k] = None
    # convert rules array to a float array
    rules = rules.astype(float)
    n_chps = len(chp_rules.keys())
    return rules, n_rules, n_chps

    import pandas as pd

def merge_csv_files(file1: str, file2: str, datetime_format1: str, datetime_format2: str, time1_name: str, time2_name: str, skiprows1: int, skiprows2: int) -> pd.DataFrame:
    """
    Merge two csv files into one pd.DataFrame.
    It will compare the timestamps and expand the dataframe with the missing timestamps.
    Input:
        file1: The path to the first csv file
        file2: The path to the second csv file
        datetime_format1: The datetime format of the first csv file
        datetime_format2: The datetime format of the second csv file
        time1_name: The name of the column with the timestamps of the first csv file
        time2_name: The name of the column with the timestamps of the second csv file
    Output:
        df: The merged pandas dataframe
    """
    # Import the csv files
    df1 = pd.read_csv(file1, delimiter=",", decimal=".", header = skiprows1)
    df2 = pd.read_csv(file2, delimiter=",", decimal=".", header = skiprows2)

    # Convert the timestamps to datetime objects
    df1[time1_name] = pd.to_datetime(df1[time1_name], format=datetime_format1)
    df2[time2_name] = pd.to_datetime(df2[time2_name], format=datetime_format2)

    # compare the timestamps if they are the same. If yes, merge the dataframes. If not, expand the dataframe with the missing timestamps and interpolate the missing values
    if not df1[time1_name].equals(df2[time2_name]):
        # Expand the dataframe with the missing timestamps
        df1 = df1.set_index(time1_name)
        df2 = df2.set_index(time2_name)
        df1 = df1.reindex(df2.index, method="ffill")
        df2 = df2.reindex(df1.index, method="ffill")
        df1 = df1.reset_index()
        df2 = df2.reset_index()
        # Interpolate the missing values
        df1 = df1.interpolate(method="ffill")
        df2 = df2.interpolate(method="ffill")
        # Merge the dataframes
        df = pd.merge(df1, df2)
    else:
        df = pd.merge(df1, df2, on=time1_name)

    return df

# power_prices_file = "/mnt/c/users/BuB/Jonas Nextcloud/Jonas_Dokumente/Arbeit/EVT_WiMi/Privater Ordner Uni/Repositories/techno-economic-assessment-methanation-on-wwtps/src/power-prices_2018.csv"
# WWTP_file = "/mnt/c/users/BuB/Jonas Nextcloud/Jonas_Dokumente/Arbeit/EVT_WiMi/Privater Ordner Uni/Repositories/techno-economic-assessment-methanation-on-wwtps/src/KW_F1.csv"
# df = merge_csv_files(power_prices_file, WWTP_file, "%d/%m/%Y %H:%M", "%Y-%m-%d %H:%M:%S", "Timestamp", "Time", 0, 0)

def expand_time_horizon(df, factor: int):
    """
    Expand the time horizon of a DataFrame by a `factor`. The new values in the dataframe are just copies of the old ones, while the time is increasing in the same intervals as the original dataframe.
    Use `apply_inflation` or `extrapolate_cyclic_data` to manipulate values in the dataframe that are changing in a predictable way.
    Arguments:
        df: pd.DataFrame with data and a column "Time / h"
        factor: int, factor to expand the DataFrame
    Returns:
        pd.DataFrame. Extended DataFrame.
    """
    # make a deep copy in order to leave the original dataframe untouched
    df_to_add = df.copy(deep=True)
    # to avoid "time going backwards"-issues, remove all rows with negative time before adding them.
    if df_to_add["Time / h"].min() <= 0:
        print("Dropping all data which is at Time <= 0")
        df_to_add = df_to_add[df_to_add["Time / h"] > 0]
    add_time = df_to_add["Time / h"]

    # make a deep copy in order to leave the original dataframe untouched
    df_concat = df.copy(deep=True)
    # build up df_concat
    for i in range(factor-1):
        max_time = df_concat["Time / h"].max()
        df_to_add["Time / h"] = add_time + max_time
        df_concat = pd.concat([df_concat, df_to_add], ignore_index=True)
    
    # fix timestamps
    start_time = df_concat["Time"][0]
    df_concat["Time"] = start_time + pd.to_timedelta(df_concat["Time / h"].to_numpy(), unit='h')

    return df_concat


def apply_inflation(orig_df: pd.DataFrame, apply_df: pd.DataFrame, col: str, inflation: float, time_span: float = 8760.0) -> pd.DataFrame:
    """
    Inflate value from `col` in `apply_df` by factor `inflation` per `time_span`
    `orig_df` is needed to distinguish between given data and extrapolated data
    Arguments:
        df: pd.DataFrame, must contain column "Time / h" and `col`
        col: str, string with column name to be inflated
        inflation: float, inflation factor (e.g. 0.05)
        time_span: float, time over which inflation occurs
    Returns:
        pd.DataFrame. DataFrame with inflated values
    """
    # make a deep copy in order to leave the original dataframe untouched
    apply_df = apply_df.copy(deep=True)

    apply_start_row = orig_df[col].shape[0]+1
    orig_time = orig_df["Time / h"][orig_df["Time / h"].shape[0]-1]
    apply_df.loc[apply_start_row:,col] = apply_df.loc[apply_start_row:,col] * (1 + (apply_df["Time / h"][apply_start_row:] - orig_time) / time_span * inflation)
    return apply_df

def extrapolate_cyclic_data(orig_df: pd.DataFrame, expand_df: pd.DataFrame, col: str, volatility: float, inflation: float, time_span: float) -> pd.DataFrame:
    """
    Extrapolate cyclic data from `col` in `expand_df` by factor `volatility` and `inflation` per `time_span` with decomposition in cyclic and fluctuating component over Weekdays
    `orig_df` is needed to distinguish between given data and extrapolated data
    Arguments:
        orig_df: pd.DataFrame, must contain columns "Time", "Time / h" and `col`
        expand_df: pd.DataFrame, must contain columns "Time", "Time / h" and `col`
        col: str, string with column name to be extrapolated
        volatility: float, volatility factor (e.g. 0.05) for fluctuating component. Increasing over `time_span`
        inflation: float, inflation factor (e.g. 0.05) for cyclic component. Increasing over `time_span`
        time_span: float, time over which inflation and volatility factor occurs
    Returns:
        pd.DataFrame. Extended DataFrame.
    """
    # make a deep copy in order to leave the original dataframe untouched
    expand_df = expand_df.copy(deep=True)
    
    orig_df["Weekday"] = pd.to_datetime(orig_df["Time"]).dt.weekday
    avg = orig_df.groupby(["Weekday"])[col].mean()

    # decompose into cyclic ("Weekday avg") and fluctuating component
    expand_df["Weekday"] = pd.to_datetime(expand_df["Time"]).dt.weekday
    expand_df["Weekday avg"] = expand_df["Weekday"].map(avg)
    expand_df["Fluctuation"] = expand_df[col] - expand_df["Weekday avg"]

    # figure out where to start extrapolating
    extrap_start_row = orig_df[col].shape[0]+1
    orig_time = orig_df["Time / h"][orig_df["Time / h"].shape[0]-1]

    # extrapolate only rows not given by original data
    expand_df.loc[extrap_start_row:,"Weekday avg"] = expand_df.loc[extrap_start_row:,"Weekday avg"] * (1 + (expand_df["Time / h"][extrap_start_row:] - orig_time) / time_span * inflation)
    expand_df.loc[extrap_start_row:,"Fluctuation"] = expand_df.loc[extrap_start_row:,"Fluctuation"] * (1 + (expand_df["Time / h"][extrap_start_row:] - orig_time) / time_span * volatility)
    
    # recompose by adding cyclic and fluctuating component
    expand_df[col] = expand_df["Weekday avg"] + expand_df["Fluctuation"]

    # clean up
    expand_df = expand_df.drop(columns=["Weekday", "Weekday avg", "Fluctuation"])
    orig_df = orig_df.drop(columns=["Weekday"])

    return expand_df