from numba import float64
from numba.experimental import jitclass

@jitclass
class ElectricityGrid:
    """
    Class to describe the electricity grid and its prices.
    TODO: In the future, this class should be used to calculate and return the electricity price (or cost/revenue of power consumption/sale) for future scenarios based on specific assumptions...
    """

    @property
    def Load(self) -> float:
        return self.__load

    @property
    def Capacity(self) -> float:
        return self.__capacity
    
    def __init__(self):
        self.__load: float = 0.0 # in KW
        self.__capacity: float = 0.0 # in KW
        
    def add_capacity(self, capacity: float):
        self.__capacity += capacity

    def draw_load(self, amount: float):
        self.__load += amount





