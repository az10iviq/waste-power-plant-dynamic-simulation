from numba.experimental import jitclass
from numba import float64

@jitclass([
    ('capacity', float64),
    ('level', float64),
    ('tendency', float64)
])
class GasStorage:
    def __init__(
        self,
        capacity: float,
        initial_level: float
    ):
        self.capacity: float = capacity
        self.level: float = initial_level
        self.tendency: float = 0.0

    def step(self):
        self.tendency = 0

    def store(self, gas_flow: float, time_diff: float):
        self.level += gas_flow * time_diff
        if self.level > self.capacity:
            self.level = self.capacity
        self.tendency += gas_flow

    def fetch(self, gas_flow: float, time_diff: float):
        self.level -= gas_flow * time_diff
        if self.level < 0:
            self.level = 0
        self.tendency -= gas_flow