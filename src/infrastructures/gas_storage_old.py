from gases import Gas
import numpy as np

class GasStorage:
    iterations = 1
    def __init__(
        self,
        gas: Gas,
        max_vol: float,
        p_in: float,
        p_store: float,
        eta: float,
        max_el_power_uptake: float,
        vol_init: float,
        capex_sp: float,
        opex_factor: float,
        capex_sp_compr: float,
        opex_factor_compr: float,
        time_arr: np.ndarray
    ):
        """
        A class that represents a gas storage.
        Arguments:
            gas: the gas that is stored
            max_vol: the maximum volume of the storage [Nm³]
            p_in: the pressure of the gas at the inlet [bar]
            p_store: the pressure of the gas in the storage [bar]
            eta: the efficiency of the compressor
            max_el_power_uptake: the maximum electrical power consumed by compressor [kW]
            capex_sp: the specific capital expenditure [€/Nm³]
            opex_factor: the factor for the operational expenditure [h^-1] (will get multiplied with CAPEX, so that it forms [€/h])
            capex_sp_compr: the specific capital expenditure for the compressor [€/Nm³]
            opex_factor_compr: the factor for the operational expenditure for the compressor [h^-1] (will get multiplied with CAPEX, so that it forms [€/h])
        """
        self.gas = gas
        self.storage = np.zeros(time_arr.shape)
        self.tendency = np.zeros(time_arr.shape)
        self.max_vol = max_vol
        self.p_in = p_in
        self.p_store = p_store
        self.eta = eta
        self.max_el_power_uptake = max_el_power_uptake
        self.vol = vol_init
        self.capex_sp = capex_sp
        self.opex_factor = opex_factor
        self.capex_sp_compr = capex_sp_compr
        self.opex_factor_compr = opex_factor_compr
        self.compr = Compressor(gas, p_in, p_store, eta, max_el_power_uptake, capex_sp_compr, opex_factor_compr)

    def run(self, gas_flow: float, t_in: float, time_diff: float):
        """
        Runs the storage.
        Arguments:
            gas_flow: the gas flow [Nm³/h]
            t_in: the temperature of the gas at the inlet [°C]
            time_diff: the time difference [h]
        """
        # if the gas flow is positive, the gas is compressed
        if gas_flow > 0:
            self.vol, el_consumption, capex_compr, opex_compr = self.store(gas_flow, t_in, time_diff)

        # if the gas flow is negative, the gas is decompressed
        else:
            self.vol -= gas_flow * time_diff
            el_consumption, capex_compr, opex_compr = 0, 0, 0

        capex_storage, opex_storage = self.calculate_cost()
        capex = capex_storage + capex_compr
        opex = opex_storage + opex_compr
        self.storage[self.iterations] = self.vol
        self.tendency[self.iterations] = gas_flow
        self.iterations += 1
        return self.vol, el_consumption, capex, opex

    def store(self, gas_flow: float, t_in: float, time_diff: float):
        """
        Stores gas in the storage.
        Arguments:
            gas_flow: the gas flow that is stored [Nm³/h]
        Returns:
            None
        """
        el_consumption, capex, opex = self.compr.run(gas_flow, t_in)

        self.vol += gas_flow * time_diff

        if self.vol > self.max_vol:
            # logging.debug("Gas storage is full.")
            pass

        return self.vol, el_consumption, capex, opex

    def calculate_cost(self):
        """
        Calculates the cost of the storage.
        """
        # calculate the capital expenditure (capex_sp: 100€/Nm³)
        capex = self.capex_sp * self.max_vol
        # calculate the operational expenditure (opex_factor: 0.05 @ 100€/Nm³)
        opex = self.opex_factor * capex

        return capex, opex


class Compressor:
    def __init__(self, gas: Gas, p_in: float, p_out: float, eta: float, max_el_power_uptake: float, capex_sp: float, opex_factor: float):
        """
        A class that represents a compressor.
        Arguments:
            gas: Gas object
            p_in: inlet pressure [bar]
            p_out: outlet pressure [bar]
            eta: efficiency [-]
            max_el_power_uptake: maximum electrical power uptake [kW]
            capex_sp: specific capital expenditure [€/Nm³]
            opex_factor: factor for the operational expenditure [h^-1] (will get multiplied with CAPEX, so that it forms [€/h])
        """
        self.gas = gas
        self.p_in = p_in
        self.p_out = p_out
        self.eta = eta
        self.max_el_power_uptake = max_el_power_uptake
        self.capex_sp = capex_sp
        self.opex_factor = opex_factor

    def run(self, gas_flow: float, t_in: float):
        """
        Runs the compressor
        Arguments:
            gas_flow: volume flow of gas [Nm³/h]
            t_in: inlet temperature [°C]
        """
        # calculate the mass flow
        m_biogas = self.gas.rho_norm * gas_flow  # [kg/h]
        # calculate the outlet temperature
        # Calculated from https://www.powderprocess.net/Tools_html/Compressors/Tools_Compressor_Power.html and https://www.quora.com/How-do-you-find-the-outlet-temperature-of-a-compressor-pump-given-input-power-inlet-pressure-and-temperature-and-outlet-pressure
        t_out = ((t_in + 273.15) * ((self.p_out / self.p_in) ** (self.gas.kappa - 1)) - (t_in + 273.15)) / self.eta + (t_in + 273.15) - 273.15
        # calculate the power needed
        # Calculated from https://onlinelibrary.wiley.com/doi/pdf/10.1002/9781118706992.app3
        el_consumption = (self.gas.cp * (t_in + 273.15) * ((self.p_out / self.p_in) ** ((self.gas.kappa - 1) / self.gas.kappa) - 1)) * m_biogas / 3600 / self.eta

        capex, opex = self.calculate_cost()

        return el_consumption, capex, opex

    def calculate_cost(self):
        """
        Calculates the cost of the compressor
        """
        # calculate the capital expenditure (capex_sp: 1500 €/kW @ 60 kW)
        capex = self.capex_sp * self.max_el_power_uptake
        # calculate the operational expenditure (opex_factor: 0.05 @ 60 kW)
        opex = self.opex_factor * capex

        return capex, opex