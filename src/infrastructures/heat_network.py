# class to model the heating network
import numpy as np

#@jitclass()
class HeatNet():

    def __init__(self, cp: float, initial_temp) -> None:
        """
        Initializes the heat network with a specific heat capacity c_p in kJ/(kg*K).
        Arguments:
            cp: float, specific heat capacity in kJ/(kg*K)
        """
        self.cp = cp
        self.temperature = initial_temp
    

    def new_temperature(self, heat: float, mass_flow: float) -> float:
        """
        Sets new temperature based on heat input, previous temperature of HeatNet and mass_flow in the heat_network
        Implements the equation: $T_{new} = T_{old} + Q/(m_dot*c_p)$
        Arguments:
            heat: float, heat input in kW
            old_temperature: float, temperature in °C
            mass_flow: float, mass flow in kg/h
        Returns:
            new_temperature: float, temperature in °C
        """
        self.temperature += heat / (mass_flow * self.cp)
    
    def calculate_heat(self, new_temperature: float, mass_flow: float) -> float:
        """
        Calculates heat output based on old and new temperature and mass flow in the heat_network
        Implements the equation: $Q = m_dot*c_p*(T_{new}-T_{old})$
        Arguments:
            new_temperature: float, temperature in °C
            mass_flow: float, mass flow in kg/h
        Returns:
            heat: float, heat output in kW
        """
        return mass_flow * self.cp*(new_temperature-self.temperature)
    
    def calculate_temperature(self, heat: float, temperature_old: float, mass_flow: float) -> float:
        """
        Calculates temperature based on heat input, previous temperature and mass_flow in the heat_network
        Implements the equation: $T_{new} = T_{old} + Q/(m_dot*c_p)$
        Arguments:
            heat: float, heat input in kW
            old_temperature: float, temperature in °C
            mass_flow: float, mass flow in kg/h
        Returns:
            new_temperature: float, temperature in °C
        """
        temperature_new = temperature_old + heat / (mass_flow * self.cp)
        return temperature_new