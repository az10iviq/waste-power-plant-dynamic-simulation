from modules.boiler import *
from modules.flare import *
from modules.chp import *
from modules.fermenter import *
from modules.electrolyzer import *
from infrastructures.heat_network import *
from infrastructures.gas_storage import *
from import_setup import *
from infrastructures.el_grid import *
from params import *
from config import *
from gases import biogas
from DataDumper import DataDumper

# region el_grid
filestr = os.path.dirname(__file__) + "/../data/data_w_benchmark_CHPs.csv"
el_grid_germany = ElectricityGrid()
df = el_grid_germany.import_historic_prices(filestr)
# TODO: this function should become more interactive over time - see el_grid.py for further info
df["Real Price / EURO/MWh"] = el_grid_germany.calculate_real_price(
    df["Price / EURO/MWh"].values)
# endregion el_grid
TOTAL_STEPS = len(df["Time / h"].values)
# region heat_network
# initialize heat_network
heat_net = HeatNet(cp=h2o_params["cp_l"], time_arr=np.zeros(TOTAL_STEPS))
# endregion

# region fermenter

fermenter = Fermenter(0.5, 0.2, 1000, np.zeros(TOTAL_STEPS), np.zeros(TOTAL_STEPS))
df = fermenter.import_data(df, filestr, "Gas Production / Nm^3/h")
df = fermenter.import_data(df, filestr, "Fermenter Heat Demand / kW")
fermenter = Fermenter(
    0, 0, 0, df["Gas Production / Nm^3/h"].values.astype(float), df["Fermenter Heat Demand / kW"].values.astype(float))
# endregion

# region chp

# write the thresholds of running modes (full load, partial load) into a dictionary (units: Nm^3, fraction). If below lowest threshold, the CHP won't run
# LoadScheduler prioritizes the rules according their order. In this example, rule1 is prioritized over rule2.
# Tendency is an additional argument. If set to >0, it means the tendency of gas storage must be positive in order to apply the rule.
# If set to <0, it means the tendency of gas storage must be negative in order to apply the rule.
# If set to 0, it means the tendency of gas storage doesn't matter in order to apply the rule.
chp_count = 4
chp_names = list(chp_gas_storage_rules_dict.keys())
# write rules in numpy arrays with import_setup.get_rules()
chp_gas_storage_rules, _, _ = get_rules(chp_gas_storage_rules_dict)
chp_maintenance_rules, num_maintenance_rules, num_chps = get_rules(
    chp_maintenance_rules_dict)
chp_efficiency_rules, _, _ = get_rules(chp_efficiency_rules_dict)


chps: list[CHP] = np.empty(chp_count, dtype=CHP)
for i, chp_name in enumerate(chp_gas_storage_rules_dict.keys()):
    chps[i] = CHP(
            chp_max_gas_power_uptake_dict[chp_name],
            chp_efficiency_rules[i],
            maintenance_time_on_failure=chp_failures_dict[chp_name][1],
            avg_time_betw_failure=chp_failures_dict[chp_name][0],
            avg_time_diff=np.mean(df["Time_diff / h"].values.astype(float)),
            time_arr=df["Time / h"].values.astype(float),
            num_maintenance_rules=num_maintenance_rules,
        )
# endregion chp

# region flare
# Nm^3 (1800 is the safety limit of the 3000 Nm^3 capacity of gas holder 1). If both tanks are available (8000 Nm^3 total volume), the threshold is 6500 Nm^3
flare_threshold = 6500
# initialize flare
flare = Flare(time_arr=df["Time / h"].values.astype(float))
# endregion

# region boiler
# TODO: Check if the value is calculated for upper or lower heating value
boiler_efficiency_rules, _, num_boilers = get_rules(
    boiler_efficiency_rules_dict)
boiler_stepless_intervals, _, _ = get_rules(boiler_stepless_intervals_dict)

# initialize boiler
boilers: list[Boiler] = np.empty(num_boilers, dtype=Boiler)
for i, boiler_name in enumerate(boiler_max_gas_power_uptake_dict.keys()):
    boilers[i] = Boiler(boiler_max_gas_power_uptake_dict[boiler_name], 
                        boiler_efficiency_rules[i], 
                        boiler_stepless_intervals[i], 
                        time_arr=df["Time / h"].values.astype(float))

# endregion boiler

# region electrolyzer

electrolyzer = Electrolyzer(**electrolyzer_init_args)


# endregion electrolyzer

# region methanation
methanation_efficiency_rules_dict = {
    "meth1": {
        # name: gas load, yield_ch4, eta_th (here, it is just the surplus heat from methanation, with 20% losses.)
        "load0":    [0, 0.875, 0.0965],
        "load0.5":  [0.5, 0.875, 0.0965],
        "load1":    [1, 0.875, 0.0965],
    },
}
# eta_th is defined as gas load [kW] / theoretical heat output [kW] * efficiency = 2461 / 297 * 0.8 = 0.0965
# heat output is calculated by \Delta H_{meth} * n^._{ch4} = 297 kW


methanation_stepless_intervals_dict = {
    "meth1": {
        # intervals for stepless control of electrolyzer
        # name, lower bound, upper bound
        "0-0":    [0, 0]
    },
}

meth_max_biogas_power_uptake_dict = {
    "meth1": 2461,  # kW of gas. Reactor is normed to take 60/40 of CH4/CO2. Corresponds to upper heating value of 371 Nm^3/h biogas of 60/40 CH4/CO2

}
# endregion methanation

gas_storage = GasStorage(
    biogas,
    2000,
    100,
    200,
    0.5,
    1000,
    2402.79,
    0.5,
    0.2,
    0.5,
    0.2,
    df["Time / h"].values.astype(float),
)


dumper = DataDumper(TOTAL_STEPS)

dumper.register(heat_net)
dumper.register(fermenter)
for chp in chps:
    dumper.register(chp)
dumper.register(flare)
for boiler in boilers:
    dumper.register(boiler)
dumper.register(electrolyzer)
# [TODO] dumper.register(methanation)
dumper.register(gas_storage)