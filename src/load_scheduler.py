from modules.chp import CHP
from modules.boiler import Boiler
from modules.electrolyzer import Electrolyzer
from modules.methanation import Methanation
from modules.flare import Flare
from modules.fermenter import Fermenter
from modules.cooler import Cooler
from infrastructures.heat_network import HeatNet
from infrastructures.gas_storage import GasStorage
from typing import Tuple
from config import GLOBAL_PARAMS as PARAMS
from config import GAS_PARAMS
from economics import Economics


import numpy as np


# REVIEW: In the future, only instances of the gas utilities (chps, boilers,...) should be imported, and no more attributes (i.e. we will replace chp_gas_storage_rules and chp_maintenance_rules by simply importing the chps-List (as a numba-List)). This will declutter the code.

# class for the operator - schedules production (according to price and gas storage volume) and maintenance of CHPs
# in order to make it compatible to numba, all self.-variable types are defined with annotations right at the beginning of the class (all other type annotations are ignored by numba, also the ones in the __init__ method. See https://numba.readthedocs.io/en/stable/user/jitclass.html)
# numba has troubles dealing with Lists. So often, using Tuples instead will fix the problem.
# Also, when defining new arrays inside, don't use Numpys dtype argument, as Numba will automatically choose a C-datatype, which is not the same as Numpys dtypes.
# nb.instance.jitclass.CHP
# numba.experimental.jitclass.base.Boiler


class LoadScheduler:
	def __init__(self, \
		chp_gas_storage_rules: np.ndarray, \
		chp_maintenance_rules: np.ndarray, \
		chps: list[CHP], \
		boilers: list[Boiler], \
		electrolyzer: Electrolyzer, \
		methanation:  Methanation, \
		gas_storage: GasStorage, \
		flare: Flare, \
		fermenter: Fermenter, \
		heat_net: HeatNet, \
		cooler: Cooler, 
		economics: Economics
		):
		"""
		TODO: please improve docstring on this class definition (e.g. explain methods shortly...)
		Arguments:
			flare_threshold: float, the gas storage volume from which the flare is starting to run
			chp_gas_storage_rules: numpy array (dimensions: (n_chps, n_rules, 2)) containing the load rules based on gas storage for each CHP. Last dimension: element 0: gas storage volume threshold, element 1: load
			chp_maintenance_rules: numpy array (dimensions: (n_chps, n_rules, 2)) containing the maintenance rules for each CHP. Last dimension: element 0: runtime threshold, element 1: maintenance duration
			chps: List, Numba-List of CHP objects
			boilers: List, Numba-List of boiler objects
			heat_net: HeatNet, HeatNet instance
			price_threshold: threshold for the price below which the CHPs can run
		"""
		self.elapsed_time = 0
		# creating some local variables
		self.chp_maintenance_rules = chp_maintenance_rules
		self.chp_gas_storage_rules = chp_gas_storage_rules
		self.net_biogas = 0
		self.net_electricity = 0
		self.net_heat = 0
		self.sng_production = 0
		self.net_water = 0

		# because the operator needs to know how much heat the boilers can produce, we need to pass the boilers List to the operator
		# this instance is a plain copy of the boilers List at the beginning of the simulation. It is not updated during the simulation.
		self.boilers = boilers
		self.gas_storage = gas_storage
		self.flare = flare
		self.fermenter = fermenter
		self.heat_net = heat_net
		self.cooler = cooler
		self.chps = chps
		self.electrolyzer = electrolyzer
		self.methanation = methanation

		self.economics = economics

		# check if all rule-tables are addressing the same number of CHPs
		if len(self.chps) != len(self.chp_maintenance_rules):
			raise ValueError("chp_maintenance_rules does not match the number of CHPs")
		if len(self.chps) != len(self.chp_gas_storage_rules):
			raise ValueError("chp_gas_storage_rules does not match the number of CHPs")

		# Helper variable to hold number of valid (not NaN) maintenance rules for each CHP. Has shape (n_chps,)
		self.num_maintenance_rules = np.count_nonzero(~np.isnan(self.chp_maintenance_rules[:, :, 0]), axis=1)
		# Helper variable to count the time a maintenance rule is active.
		self.maintenance_time = np.zeros((len(self.chps), self.chp_maintenance_rules.shape[1]))
		# Helper variable to count the time the CHP is active between two maintenance intervals. Has shape (n_chps,n_rules)
		self.run_time_between_maintenance = np.zeros((len(self.chps), self.chp_maintenance_rules.shape[1]))

		# Helper variable to check if gas storage rules are active. Has shape (n_chps, n_rules)
		# self.is_gas_storage = np.zeros((len(self.chps), chp_gas_storage_rules.shape[1]))
		# Helper variable to hold number of valid (not NaN) gas storage rules for each CHP. Has shape (n_chps,)
		self.num_gas_storage_rules = np.count_nonzero(~np.isnan(self.chp_gas_storage_rules[:, :, 0]), axis=1)
		# Helper variable to indicate if specific gas storage rules are active. Has shape (n_chps, n_rules)
		self.gas_storage_above_threshold = np.zeros(shape=(len(self.chps), chp_gas_storage_rules.shape[1]))


		# Helper variable to decide if it is economically feasible to run the CHPs
		self.price_below_threshold : bool = False


		self.set_gas_storage_rules(chp_gas_storage_rules)
		self.set_maintenance_rules(chp_maintenance_rules)




	def schedule_production(self, new_time_diff: float, new_price : float, mass_flow_heat_net: float, temp_threshold: np.ndarray, chps, boilers, electrolyzer, methanation, gas_storage, flare, fermenter, heat_net, cooler):
		"""
		TODO: improve docstring on this function
		Since maintenance and price are only implemented for CHPs, the variables are not named specifically for CHPs (e.g. old_maintenance is implicitly the old maintenance of the CHPs)
		Might have to be changed in the future (different price thresholds for CHPs and Electrolyzers, different maintenance rules for CHPs and Electrolyzers)
		## schedule_production
		### Takes arguments:
			new_time_diff : float
			new_price : float
			mass_flow_heat_net: float
			temp_threshold: np.ndarray with 2 entries. Lower threshold for the temperature below which the boilers need to start and upper threshold for when the coolers need to run (in °C)

		### Returns:
			self.gas_storage_above_threshold
			self.price_below_threshold
		"""
		chp_heat_production = 0.0
		boiler_heat_production = 0.0

		if new_price < PARAMS.PRICE_THRESHOLD:
			self.price_below_threshold = True
		else:
			self.price_below_threshold = False

		# region CHPs
		for i, chp in enumerate(chps):
			for j in range(self.num_maintenance_rules[i]):
				if (chp.TimeSinceLastMaintenance >= self.chp_maintenance_rules[i,j,0]) & (chp.TotalMaintenanceTime % self.chp_maintenance_rules[i,j,1] == 0):
					chp.RemainingMaintenanceTime += self.chp_maintenance_rules[i,j,1]

			for j in np.arange(self.num_gas_storage_rules[i]-1,-1,-1):
				if (gas_storage.level > self.chp_gas_storage_rules[i,j,0]) & (gas_storage.tendency * self.chp_gas_storage_rules[i,j,1] >= 0):
					self.gas_storage_above_threshold[i,j] = True
					chp.Load = self.chp_gas_storage_rules[i,j,2]
				else:
					self.gas_storage_above_threshold[i,j] = False
			if not np.any(self.gas_storage_above_threshold[i,:]):
				# logging.debug("Gas Storage too low")
				pass

			if (self.price_below_threshold) & (np.any(self.gas_storage_above_threshold[i,:])) & (chp.RemainingMaintenanceTime >= 0):
				_, helper_heat = chp.Products # heat produced by CHP in previous iteration
				chp_heat_production += helper_heat
			else:
				chp.Load = 0
		# endregion

		if gas_storage.level >= PARAMS.FLARE_THRESHOLD:
			flare.Load = self.flare.calculate_load(gas_storage.level - PARAMS.FLARE_THRESHOLD)
		else:
			flare.Load = 0.0
		

		temperature_after_chps = heat_net.calculate_temperature(chp_heat_production, heat_net.temperature, mass_flow_heat_net)
		# logging.debug("new_temp_heat_net: " + str(new_temp_heat_net))
		# if new_temp_heat_net is below temp_threshold, additional heat must be produced by boilers
		if temperature_after_chps < temp_threshold[0]:
			# logging.debug("Temperature of heat net would be too low")
			# calculate how much heat must be produced by boilers
			additional_heat_needed = heat_net.calculate_heat(temp_threshold[0], mass_flow_heat_net) - chp_heat_production
			# logging.debug("heat_needed: " + str(heat_needed))
			# TODO: put this statement inside separate method "calculate_boiler_load" to declutter the code
			# now calculate the boiler load
			for boiler in boilers:
				boiler_heat_production = float(boiler.Products[0])
				additional_heat_needed -= boiler_heat_production
				boiler.Load = boiler.calculate_load(additional_heat_needed)
				# avoid unnecessary calculations
				if additional_heat_needed < 1e-3:
					break
		
		if temperature_after_chps > temp_threshold[1]:
			excess_heat = heat_net.calculate_heat(temperature_after_chps, mass_flow_heat_net) - \
				heat_net.calculate_heat(temp_threshold[1], mass_flow_heat_net)
			cooler.Load = cooler.calculate_load(excess_heat)

		self.elapsed_time += new_time_diff

		return self.gas_storage_above_threshold, self.price_below_threshold

	def update_production_statistics(self, net_biogas, net_electricity, net_heat, sng_production, water_usage):
		self.sng_production += sng_production
		self.net_electricity += net_electricity
		self.net_biogas += net_biogas
		self.net_heat += net_heat
		self.net_water += water_usage
	   


	def set_gas_storage_rules(self, chp_gas_storage_rules):
		"""
		Re-Set the gas storage rules for the CHPs after initialization
		"""

		self.chp_gas_storage_rules = chp_gas_storage_rules
		# Helper variable to hold number of valid (not NaN) gas storage rules for each CHP. Has shape (n_chps,)
		self.num_gas_storage_rules = np.count_nonzero(~np.isnan(self.chp_gas_storage_rules[:, :, 0]), axis=1)
		# Helper variable to indicate if specific gas storage rules are active. Has shape (n_chps, n_rules)
		self.gas_storage_above_threshold = np.zeros(shape=(len(self.chps), chp_gas_storage_rules.shape[1]))

		# check if all rule-tables are addressing the same number of CHPs
		if len(self.chps) != len(self.chp_maintenance_rules):
			raise ValueError("chp_maintenance_rules and chp_gas_storage_rules must address the same number of CHPs")

	def set_maintenance_rules(self, chp_maintenance_rules):
		"""
		Re-Set the maintenance rules for the CHPs after initialization
		"""
		# Helper variable to hold number of valid (not NaN) maintenance rules for each CHP. Has shape (n_chps,)
		self.num_maintenance_rules = np.count_nonzero(~np.isnan(chp_maintenance_rules[:, :, 0]), axis=1)
		# Helper variable to count the time a maintenance rule is active.
		self.maintenance_time = np.zeros((len(self.chps), chp_maintenance_rules.shape[1]))
		# Helper variable to count the time the CHP is active between two maintenance intervals. Has shape (n_chps,n_rules)
		self.run_time_between_maintenance = np.zeros((len(self.chps), chp_maintenance_rules.shape[1]))

	def get_economic_stats(self, net_electricity, net_oxygen, time):
		"""
		Calculate the economic statistics for the current time step
		### Returns:
		- gross cash flow
		- net cash flow
		- net present value
		"""
		return self.economics.calculate_economic_stats(net_electricity, net_oxygen, time)
