# refactored boiler module
import numpy as np
import sys
sys.path.append('src/')
from gases import GasMix
from modules.module import Module
from numba import float64, boolean, int32
from numba.experimental import jitclass

@jitclass(spec=[
    # ============ Module ============
    ('_global_time', float64),
    ('_runtime', float64),
    ('_remaining_maintenance_time', float64),
    ('_products', float64[:]),
    ('_consumption', float64[:]),
    ('_time_since_last_maintenance', float64),
    ('_total_maintenance_time', float64),
    ('_maintenance_cost_per_hour', float64),
    ('_MTTF', float64),
    ('_MTTR', float64),
    ('_under_maintenance', boolean),
    ('_load', float64),
    # ============ Boiler ============
    ('max_output_capacity', float64),
    ('efficiency_rules', float64[:,:]),
    ('stepless_bounds', float64[:,:]),
    ('power_output_with_efficiency', float64[:]),
    ('power_output_with_loads', float64[:]),
    ('maintenance_cost_per_hour', float64),
    ('capex_sp', float64),
    ('capex', int32),
    ('_fuel', GasMix.class_type.instance_type)
])
class Boiler(Module):
    
    def __init__(self, max_output_capacity: int, efficiency_rules: np.ndarray, stepless_bounds: np.ndarray,\
                  MTTR: float, \
				  maintenance_cost_per_hour: float,\
                  capex_sp:float, \
                  capex: int,
                  fuel: GasMix) -> None:

        self.max_output_capacity = max_output_capacity
        self.efficiency_rules = efficiency_rules
        self.stepless_bounds = stepless_bounds
        self._MTTR = MTTR
        self._maintenance_cost_per_hour = maintenance_cost_per_hour
        self.capex_sp = capex_sp
        self.capex = capex
        self._products = np.array([0.0])
        self._consumption = np.array([0.0])
        self._fuel = fuel

        self.power_output_with_efficiency = self.efficiency_rules[:, 1] * self.max_output_capacity
        self.power_output_with_loads = self.efficiency_rules[:, 0] * self.power_output_with_efficiency

    def calculate_load(self, power_demand: float) -> float:
        if power_demand == 0.0:
            return float(0.0)
        elif power_demand > self.max_output_capacity:
            return float(1.0)
        else:
            closest_rule = np.argmin(np.abs(self.power_output_with_loads - power_demand))
            for bound in self.stepless_bounds:
                if bound[0] <= self.efficiency_rules[closest_rule, 0] <= bound[1]:
                    return float((power_demand * self.efficiency_rules[closest_rule, 1]) / self.power_output_with_efficiency[closest_rule])
            return float(self.efficiency_rules[closest_rule, 0])

    def _produce(self, time_delta: float) -> np.ndarray:
        closest_rule = np.argmin(np.abs(self.efficiency_rules[:, 0] - self.Load))
        return np.array([self.Load * self.power_output_with_efficiency[closest_rule]])

    def _consume(self, time_delta: float) -> np.ndarray:
        return np.array([self.Load * self.max_output_capacity / self._fuel.h_u])

    def _calculate_maintenance_time(self) -> float:
        return self._MTTR

    def _check_failure(self) -> bool:
        return False

## Economic calculation
	# Annual cost calculation
    def maintenance_cost(self):
        return self._maintenance_cost_per_hour * self.TotalMaintenanceTime

    def debt_payment(self):
        debt_ratio = 1
        bank_interest_rate = 0.05
        payback_time = 10
        return self.capex_sp * debt_ratio * ( bank_interest_rate )* 1/(1-1/(1+bank_interest_rate)**(payback_time))
        
    def total_annual_cost(self):
        return self.maintenance_cost()
