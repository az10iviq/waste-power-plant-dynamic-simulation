from random import random
import numpy as np
from gases import GasMix
from modules.module import Module
from numba import float64, boolean, int32
from numba.experimental import jitclass
from infrastructures.gas_storage import GasStorage
from infrastructures.el_grid import ElectricityGrid
from infrastructures.heat_network import HeatNet

@jitclass(spec=[
    # ============ Module ============
    ('_global_time', float64),
    ('_runtime', float64),
    ('_remaining_maintenance_time', float64),
    ('_products', float64[:]),
    ('_consumption', float64[:]),
    ('_time_since_last_maintenance', float64),
    ('_total_maintenance_time', float64),
    ('_maintenance_cost_per_hour', float64),
    ('_MTTF', float64),
    ('_MTTR', float64),
    ('_under_maintenance', boolean),
    ('_load', float64),
    # ============ CHP ============
    ('max_gas_power_uptake', int32),
    ('efficiency_rules', float64[:,:]),
    ('avg_time_diff', float64),
    ('_stochastic_failure', boolean),
    ('p', float64[:,:]),
    ('capex', int32),
    ('_fuel', GasMix.class_type.instance_type)
])
class CHP(Module):
    def __init__(
        self,
        max_gas_power_uptake: int,
        efficiency_rules: np.ndarray,
        MTTF: int,
        MTTR: int,
        avg_time_diff: float,
        capex: int,
        fuel: GasMix
    ):
        self.max_gas_power_uptake = max_gas_power_uptake
        self.efficiency_rules = efficiency_rules
        self._MTTF = MTTF
        self._MTTR = MTTR
        self.avg_time_diff = avg_time_diff
        self._stochastic_failure = False
        self._products = np.array([0., 0.])
        self._consumption = np.array([0.])
        self.p = np.zeros((round((4 * self._MTTF) / self.avg_time_diff), 1))

        q = 1  # cumulative probability
        for i in range(self.p.shape[0]): 
            self.p[i] = (i - 1) * q / (0.5 * round(self._MTTF / self.avg_time_diff) * (round(self._MTTF / self.avg_time_diff) + 1)) / np.prod(1 - self.p[: i - 1]) # type: ignore
        self.capex = capex
        self._fuel = fuel
        
    def _produce(self, time_delta: float) -> np.ndarray:
        closest_rule = np.argmin(np.abs(self.efficiency_rules[:, 0] - self.Load))
        el_power = self.max_gas_power_uptake * self.efficiency_rules[closest_rule, 1] * self.Load
        th_power = self.max_gas_power_uptake * self.efficiency_rules[closest_rule, 2] * self.Load
        return np.array([el_power, th_power])

    def _consume(self, time_delta: float) -> np.ndarray:
        return np.array([self.Load * (self.max_gas_power_uptake / self._fuel.h_u)]) # [kW / (kWh/Nm^3) = Nm^3/h]

    def _calculate_maintenance_time(self) -> float:
        return self._MTTR

    def _check_failure(self) -> bool:
        if self.TimeSinceLastMaintenance > 0:
            if self._stochastic_failure:
                if np.random.random() < self.p[round(self.TimeSinceLastMaintenance / self.avg_time_diff)]:
                    return True
                else:
                    return False

            # if not stochastic, the plant will fail when the time between maintenance is greater than the average time between failure
            else:
                if self.TimeSinceLastMaintenance > self._MTTF:
                    return True
                else:
                    return False
        else:
            return False

    @property
    def Opex(self) -> float:
        return 0.2 * self.capex