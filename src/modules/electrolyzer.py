from numba import float64, boolean, types, int32
from numba.experimental import jitclass
from gases import Gas
from modules.module import Module
import numpy as np

@jitclass(spec=[
    # ============ Module ============
    ('_global_time', float64),
    ('_runtime', float64),
    ('_remaining_maintenance_time', float64),
    ('_products', float64[:]),
    ('_consumption', float64[:]),
    ('_time_since_last_maintenance', float64),
    ('_total_maintenance_time', float64),
    ('_maintenance_cost_per_hour', float64),
    ('_MTTF', float64),
    ('_MTTR', float64),
    ('_under_maintenance', boolean),
    ('_load', float64),
    # ============ Electrolyzer ============
	('max_el_power_uptake', float64),
	('efficiency_rules', float64[:,:]),
	('stepless_intervals', float64[:,:]),
	('h2_production_w_efficiencies', float64[:]),
	('o2_production_w_efficiencies', float64[:]),
	('o2_production_w_loads', float64[:]),
	('h2o_consumption_w_efficiencies', float64[:]),
	('failure_rate', float64),
	('capex', int32),
	('h2', Gas.class_type.instance_type),
	('h2o', Gas.class_type.instance_type)
])
class Electrolyzer(Module):
	def __init__(self, max_el_power_uptake: float, \
					h2: Gas, h2o: Gas, \
					efficiency_rules: np.ndarray, \
					stepless_intervals: np.ndarray, \
					failure_rate: float, \
					maintenance_cost_per_hour: float, \
					MTTF: float, \
					MTTR: float, capex: int):
		self.max_el_power_uptake = max_el_power_uptake
		self.h2 = h2
		self.h2o = h2o
		self.efficiency_rules = efficiency_rules
		self.stepless_intervals = stepless_intervals
		# pre-calculate the o2 and h2 output for each efficiency rule
		self.h2_production_w_efficiencies = self.efficiency_rules[:, 1] * self.max_el_power_uptake / h2.h_o
		self.o2_production_w_efficiencies = self.h2_production_w_efficiencies / 2
		# pre-calculate the o2 output for each load
		self.o2_production_w_loads = np.multiply(self.efficiency_rules[:, 0], self.o2_production_w_efficiencies)
		# pre-calculate the liquid h2o consumption for each efficiency rule
		self.h2o_consumption_w_efficiencies = self.h2_production_w_efficiencies * self.h2.rho_norm / self.h2.M * self.h2o.M / self.h2o.rho_l
		self.failure_rate = failure_rate
		self._maintenance_cost_per_hour = maintenance_cost_per_hour
		self._MTTF = MTTF
		self._MTTR = MTTR
		self._products = np.array([0., 0.])
		self._consumption = np.array([0., 0.])
		self.capex = capex
	

	def calculate_load(self, o2_demand: float):
		# find the index of the load rule that is closest but greater or equal (geq) than the power demand
		# first, calculate the difference between the demand and O2-Production with all load rules
		o2_demand_with_rules = self.o2_production_w_loads - o2_demand
		# check_array is setting all negative elements to np.inf. This is necessary to find the index of the smallest element
		# if all elements are negative, the power demand is greater than the maximum power output of the boiler
		# in this case, the check_array contains only np.infs. further, load needs to be 1
		check_array = np.where(o2_demand_with_rules >= 0, o2_demand_with_rules, np.inf)
		if np.all(np.isinf(check_array)):
			load = 1.0
			return load
		else:
			closest_rule_idx = np.argmin(check_array) # returns the index of the closest load in self.electrolyzer_efficiency_rules
			# check if closest_rule_idx is in a stepless interval (that means, if closest load is geq than the lower bound and leq than the upper bound of any stepless interval).
			if np.any(np.logical_and(self.electrolyzer_stepless_intervals[:, 0] <= self.electrolyzer_efficiency_rules[closest_rule_idx, 0], self.electrolyzer_efficiency_rules[closest_rule_idx, 0] <= self.electrolyzer_stepless_intervals[:, 1])):
				# now calulate the load based on power needed and efficiency, but limit maximum load to 1
				load = min(o2_demand / (self.o2_production_w_efficiencies[closest_rule_idx]), 1)
			else:
				# if not, just take the closest load
				load = self.efficiency_rules[closest_rule_idx, 0]
		return load

	def _consume(self, time_delta: float) -> np.ndarray:
		"""
		Calculates the electricity and water consumption based on the load
		Arguments:
			load: float, electricity load as a value between 0 and 1
		Returns:
			list:
				el_consumption: float, electricity consumption in kW_el
				h2o_consumption: float, liquid water consumption in m^3/h
		"""
		closest_rule = np.argmin(np.abs(self.efficiency_rules[:, 0] - self._load))
		el_consumption = self._load * self.max_el_power_uptake
		# h2o_consumption uses the following formulas:
		# h2_production '/' h2_density_norm = h2_production_mass
		# h2_production_mass / h2_molar_weight * h2o_molar_weight = h2o_consumption_mass
		# h2o_consumption_mass / h2o_density = h2o_consumption_volume
		h2o_consumption = self._load * self.h2o_consumption_w_efficiencies[closest_rule]
		return np.array([el_consumption, h2o_consumption])

	def _produce(self, time_delta: float) -> np.ndarray:
		"""
		calculates o2 and h2 output as well as thermal power output based on the load of the electrolyzer.
		Returns:
			list:
				o2_production: float, o2 production in Nm^3/h
				h2_production: float, h2 production in Nm^3/h
				th_power: float, thermal power output of surplus heat in kW_th
		"""
		# Find closest rule to approximate conversion efficiency at specific load
		closest_rule = np.argmin(np.abs(self.efficiency_rules[:, 0] - self._load))
		h2_power = self._load * self.efficiency_rules[closest_rule, 1] * self.max_el_power_uptake
		th_power = self._load * self.efficiency_rules[closest_rule, 2] * self.max_el_power_uptake
		h2_production = h2_power / self.h2.h_o

		return np.array([h2_production/2, h2_production, th_power])

	def _calculate_maintenance_time(self) -> float:
		return np.random.exponential(self._MTTR)
	
	def _check_failure(self) -> bool:
		# Reliability of the Module in time t for a poisson process (e ^ -(failure_rate * t))
		R = np.exp(-1 * (self.failure_rate * self.TimeSinceLastMaintenance))
		# returns true with the probability of (1 - R)
		return np.random.uniform(0, 1) > R