import pandas as pd
import numpy as np
import logging

class Fermenter:
    """
    Class that describes the fermenter.
    - Imports Data (from file). 
    - TODO: In the future the Data might be described by some other function / changing over time
    - TODO: Implement heat consumption by Fermenter
    TODO: Improve docstring.
    """

    def __init__(self, capex_sp: float, opex_factor: float, max_thermal_power_capacity: float, gas_production: np.ndarray, heat_demand: np.ndarray, capex: int) -> None:
        """
        TODO: Improve docstring.
        Input:
            capex_sp: Capital expenditure specific price in EUR/kW
            opex_factor: Operational expenditure factor in EUR/kWh
            max_thermal_power_capacity: Maximum thermal power capacity in kW
            gas_production: Array with the gas production in m^3/h
            heat_demand: Array with the heat demand in kW
        """
        self.capex_sp = capex_sp
        self.opex_factor = opex_factor
        self.max_thermal_power_capacity = max_thermal_power_capacity
        self.gas_production = gas_production
        self.heat_demand = heat_demand
        self.capex = capex

    def import_data(self, df: pd.DataFrame, csvfile: str, target_col: str) -> pd.DataFrame:
        """
        Import historic data from csv file into a pandas dataframe analogue to import_historic_prices in el_grid.py
        Input:
            csvfile: The path to the csv file with the historic Data. The file needs columns with name "Time" (formatted as "%Y-%m-%d %H:%M:%S") and target_col
            df: The dataframe to which the historic Data is added. Needs column with name "Time / h"
            target_col: The name of the column in which the historic Data is added
        Output:
            df: The pandas dataframe with the historic Data
        """
        # Import historic Data from csv file
        # For further information of the procedure, look at the comments in el_grid.py
        df_gp = pd.read_csv(csvfile, delimiter=",", decimal=".")
        df_gp["Time"] = pd.to_datetime(df_gp["Time"], format="%Y-%m-%d %H:%M:%S") # format input Timestamps so pandas can understand it
        df_gp["Time / h"] = pd.to_timedelta(df_gp["Time"]-df_gp["Time"][0]) / pd.to_timedelta(1.0, unit='hr') + 0.25


        if not df_gp["Time / h"].equals(df["Time / h"]):
            raise ValueError("The time steps of the new Data and the simulation do not match. Please check the input files.")
        
        # Check if df[target_col] exists. If yes, print a warning and return the original dataframe (skip). If not, fill the column with the historic Data. 
        if target_col in df.columns:
            logging.debug(f"The column {target_col} is already filled. I will skip the import.")
            return df
        else:
            df[target_col] = df_gp[target_col]
            return df
