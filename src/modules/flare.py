import numpy as np
from modules.module import Module
from numba import float64, boolean, int32
from numba.experimental import jitclass

@jitclass(spec=[
    # ============ Module ============
    ('_global_time', float64),
    ('_runtime', float64),
    ('_remaining_maintenance_time', float64),
    ('_products', float64[:]),
    ('_consumption', float64[:]),
    ('_time_since_last_maintenance', float64),
    ('_total_maintenance_time', float64),
    ('_maintenance_cost_per_hour', float64),
    ('_MTTF', float64),
    ('_MTTR', float64),
    ('_under_maintenance', boolean),
    ('_load', float64),
    # ============ Flare ============
    ('max_gas_uptake', int32),
    ('capex', int32),
])
class Flare(Module):
    def __init__(self, capex: int, max_gas_uptake: int):
        self.capex = capex
        self.max_gas_uptake = max_gas_uptake
        self._consumption = np.array([0.])
        self._products = np.array([0.])

    def calculate_load(self, gas_demand: float) -> float:
        if gas_demand == 0.0:
            return float(0.0)
        elif gas_demand > self.max_gas_uptake:
            return float(1.0)
        else:
            return float(gas_demand / self.max_gas_uptake)

    def _produce(self, time_delta: float) -> np.ndarray:
        return np.array([0.])
    def _consume(self, time_delta: float) -> np.ndarray:
        return np.array([self.Load * self.max_gas_uptake])

    def _calculate_maintenance_time(self) -> float:
        return 0.

    def _check_failure(self) -> bool:
        return False
