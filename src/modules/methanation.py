# This should be extended to a more object-oriented approach. Also, methanation reactions etc. should be added as well as some specific CAPEX etc... see the operator.py file for a template.
#from init import *
from gases import sng, ch4, h2, biogas
from helpers import si
from pandas import DataFrame
import numpy as np


# region Constants
SNG = 0
PWR = 1
# endregion Constants


class Methanation:
    statistics: DataFrame = DataFrame({
        "cng production": [0],
        "power consumption": [0],
    })
    def __init__(self, max_biogas_power_uptake: float, \
                       efficiency_rules: np.ndarray, \
                       stepless_intervals: np.ndarray, capex: float, ):
        """
        Class for Methanation objects. It will be used to calculate CAPEX, OPEX, H2, H2O, CO2, CH4 and electricity consumption, CH4, H2 and CO2 production as well as surplus heat of the Methanation process.
        To save computation time, the SNG composition is passed only during initialization and assumed to be constant.
        However, this can be changed by passing the SNG composition to the run method.
        """
        self.meth_max_biogas_power_uptake = max_biogas_power_uptake
        self.methanation_efficiency_rules = efficiency_rules
        self.methanation_stepless_intervals = stepless_intervals
        self.ch4_b_consumption_w_loads = self.methanation_efficiency_rules [:,0] * self.meth_max_biogas_power_uptake * (biogas.ch4_frac / (biogas.ch4_frac + biogas.h2_frac)) / ch4.h_o
        self.biogas_consumption_w_loads = self.ch4_b_consumption_w_loads / biogas.ch4_frac
        self.h2_b_consumption_w_loads = self.biogas_consumption_w_loads * biogas.h2_frac
        self.co2_b_consumption_w_loads = self.biogas_consumption_w_loads * biogas.co2_frac
        self.h2_el_consumption_w_loads = self.co2_b_consumption_w_loads * 4 * 1 * h2.h_o - self.h2_b_consumption_w_loads
        self.conversion_power = (self.h2_b_consumption_w_loads + self.h2_el_consumption_w_loads) * h2.h_o * 1 
        self.co2_b_consumption_w_loads = self.biogas_consumption_w_loads * biogas.co2_frac
        self.ch4_sng_production_w_efficiencies = np.multiply(self.co2_b_consumption_w_loads, self.methanation_efficiency_rules[:,1]) + self.ch4_b_consumption_w_loads
        self.ch4_syn_production_w_efficiencies = self.ch4_sng_production_w_efficiencies - self.ch4_b_consumption_w_loads
        self.h2_production_w_efficiencies = self.h2_el_consumption_w_loads + self.h2_b_consumption_w_loads - self.ch4_syn_production_w_efficiencies * 4 * 1
        self.co2_production_w_efficiencies = self.co2_b_consumption_w_loads - self.ch4_syn_production_w_efficiencies * 1
        self.th_power_w_efficiencies = np.multiply(self.methanation_efficiency_rules [:,0], self.methanation_efficiency_rules[:,1]) * self.meth_max_biogas_power_uptake
        self.el_consumption_w_loads = self.methanation_efficiency_rules [:,0] * 2.5 * (self.meth_max_biogas_power_uptake / 20 )
        self.capex = capex

    
    def _consume(self, load: float):
        """
        Calculates the H2, biogas and electricity consumption of the Methanation process.
        Returns:
            h2_el_consumption: float, H2 consumption in Nm3/h
            biogas_consumption: float, biogas consumption in Nm3/h
            el_consumption: float, electricity consumption in kW_el
          
        """
        closest_rule = np.argmin(np.abs(self.methanation_efficiency_rules[:, 0] - load))
        self.ch4_b_consumption_w_loads
        self.h2_b_consumption_w_loads 
        self.h2_el_consumption_w_loads
        self.co2_b_consumption_w_loads

        h2_el_consumption = self.h2_el_consumption_w_loads[closest_rule]
        biogas_consumption = self.biogas_consumption_w_loads[closest_rule]
        el_consumption = self.el_consumption_w_loads[closest_rule]

        return [h2_el_consumption, biogas_consumption, el_consumption]


  

    def _produce(self, load):
        """
        Calculates the SNG production of the Methanation process.
        Arguments:
            load: float, load of the methanation process
            sng: GasMix, SNG composition
        Returns:
            sng_production: float, SNG production in Nm3/h
            h2o_production: float, vapor H2O production in Nm3/h
            th_power: float, surplus heat of the methanation process in kW
        """
        closest_rule = np.argmin(np.abs(self.methanation_efficiency_rules[:, 0] - load))
        ch4_sng_production = self.ch4_sng_production_w_efficiencies[closest_rule]
        h2_production = self.h2_production_w_efficiencies[closest_rule]
        co2_production = self.co2_production_w_efficiencies[closest_rule]
        th_power = self.th_power_w_efficiencies[closest_rule]
        h2o_production = 0
        sng_production = ch4_sng_production * sng.ch4_frac + h2_production * sng.h2_frac + co2_production * sng.co2_frac + h2o_production * sng.h2o_frac
       
        return [sng_production, h2o_production, th_power]
        
    
    def calculate_load(self, h2_demand: float):
        """
        Calculates the load of the Methanation process.
        Arguments:
            h2_consumption: float, available H2 in Nm3/h
            sng: GasMix, SNG composition
        Returns:
            load: float, load of the methanation process
        """
        # find the index of the load rule that is closest but greater or equal (geq) than the power demand
        # first, calculate the difference between the demand and O2-Production with all load rules
        h2_demand_with_rules = self.h2_el_consumption_w_loads - h2_demand
        # check_array is setting all negative elements to np.inf. This is necessary to find the index of the smallest element
        # if all elements are negative, the power demand is greater than the maximum power output of the boiler
        # in this case, the check_array contains only np.infs. further, load needs to be 1
        check_array = np.where(h2_demand_with_rules >= 0, h2_demand_with_rules, np.inf)
        if np.all(np.isinf(check_array)):
            load = 1.0
            return load
        else:
            closest_rule_idx = np.argmin(check_array) # returns the index of the closest load in self.methanation_efficiency_rules
            # check if closest_rule_idx is in a stepless interval (that means, if closest load is geq than the lower bound and leq than the upper bound of any stepless interval).
            if np.any(np.logical_and(self.methanation_stepless_intervals[:, 0] <= self.methanation_efficiency_rules[closest_rule_idx, 0], self.methanation_efficiency_rules[closest_rule_idx, 0] <= self.methanation_stepless_intervals[:, 1])):
                # now calulate the load based on power needed and efficiency, but limit maximum load to 1
                load = min(h2_demand / (self.h2_el_consumption_w_loads[closest_rule_idx]), 1)
            else:
                # if not, just take the closest load
              return  self.methanation_efficiency_rules[closest_rule_idx, 0]
        



# meth = Methanation(
#     meth_max_biogas_power_uptake_dict["meth1"], methanation_efficiency_rules[0], methanation_stepless_intervals[0], biogas_obj, sng_obj, ch4_obj, h2_obj, h2o_obj, 8, capex_sp=1000, opex_factor=0.05
# )
# closest_rule_idx = 0
# load = meth.calculate_load(0.5)
# load = 1
# h2_el_consumption, h2o_consumption, biogas_consumption, el_consumption = meth.calculate_consumption(load)
# sng_production, h2o_production, th_power = meth.calculate_output(load)

# print(sng_production, h2o_production, th_power)

# print(meth.run(load=1, meth_run_time_between_maintenance=np.zeros((2,)), meth_maintenance_remaining_failure=0, meth_new_run_time=1, time_diff=0.25))
