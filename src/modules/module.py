import numpy as np

class Module():
    def __init__(self) -> None:
        self._global_time: float = 2.0
        self._runtime: float = 0.0
        self._remaining_maintenance_time: float = 0.0
        self._time_since_last_maintenance: float = 0.0
        self._under_maintenance: bool = False
        self._total_maintenance_time: float = 0.0
        self._maintenance_cost_per_hour: float = 0.0
        self._MTTF: float = 0.0 # Mean Time To Failure
        self._MTTR: float = 0.0 # Mean Time To Repair
        self._load: float = 0.0

    @property
    def Runtime(self) -> float:
        return self._runtime

    @property
    def Load(self) -> float:
        return self._load

    @Load.setter
    def Load(self, value: float) -> None:
        self._load = value

    @property
    def TotalMaintenanceTime(self) -> float:
        return self._total_maintenance_time

    @TotalMaintenanceTime.setter
    def TotalMaintenanceTime(self, value: float) -> None:
        self._total_maintenance_time = value

    @property
    def RemainingMaintenanceTime(self) -> float:
        return self._remaining_maintenance_time

    @RemainingMaintenanceTime.setter
    def RemainingMaintenanceTime(self, value: float) -> None:
        self._remaining_maintenance_time = value
        if self._remaining_maintenance_time <= 0:
            self._remaining_maintenance_time = 0
            self._under_maintenance = False
            self._time_since_last_maintenance = 0.0
        else:
            self._under_maintenance = True
            self._load = 0.0

    @property
    def TimeSinceLastMaintenance(self) -> float:
        return self._time_since_last_maintenance

    @property
    def UnderMaintenance(self) -> bool:
        return self._under_maintenance

    @UnderMaintenance.setter
    def UnderMaintenance(self, value: bool) -> None:
        self._under_maintenance = value

    @property
    def Products(self) -> np.ndarray:
        return self._products

    @property
    def Consumption(self) -> np.ndarray:
        return self._consumption

    @property
    def CapeX(self) -> float:
        """
        Returns the capital expenditure of the module.
        """
        pass
    
    @property
    def Opex(self) -> float:
        """
        Returns the operational expenditure of the module.
        """
        pass

    def _check_failure(self) -> bool:
        """
        Checks if the module has failed.
        Returns:
            failed: bool, True if the module has failed, False otherwise
        """
        pass

    
    # [TODO] Guess time_delta is not required for produce and consume functions.
    def _produce(self, time_delta: float) -> np.ndarray:
        """
        Produces energy based on the load and time delta.
        Arguments:
            load: float, load as a value between 0 and 1
        Returns:
            products: list, list of products produced by the module
        """
        pass

    def _consume(self, time_delta: float) -> np.ndarray:
        """
        Consumes energy based on the load and time delta.
        Arguments:
            load: float, load as a value between 0 and 1
        Returns:
            products: list, list of products consumed by the module
        """
        pass

    def _maintain(self, time_delta: float):
        """
        Maintains the module based on the time delta.
        Arguments:
            time_delta: float, time difference in hours
        """
        self.RemainingMaintenanceTime -= time_delta

    def _calculate_maintenance_time(self) -> float:
        """
        Calculates the maintenance time of the module.
        Returns:
            maintenance_time: float, maintenance time in hours
        """
        pass

    def report_status(self) -> np.ndarray:
        """
        Reports the status of the module.
        Returns:
            status: list, list of status parameters
        """
        status = [
            self._load,
            self._remaining_maintenance_time,
            *self._products,
            *self._consumption,
        ]
        return np.array(status)

    def step(self, time_delta: float):
        """
        Updates the module based on the load and time delta.
        Arguments:
            load: float, load as a value between 0 and 1
            time_delta: float, time difference in hours
        """
        self._global_time += time_delta
        if not self._under_maintenance:
            if self._check_failure():
                self.RemainingMaintenanceTime = self._calculate_maintenance_time()
                self._total_maintenance_time += self.RemainingMaintenanceTime
                self._maintain(time_delta)
                self._products = np.zeros_like(self._products)
                self._consumption = np.zeros_like(self._consumption)
            else:
                self._time_since_last_maintenance += time_delta
                self._runtime += time_delta
                self._products = self._produce(time_delta)
                self._consumption = self._consume(time_delta)
        else:
            self._maintain(time_delta)
            self._products = np.zeros_like(self._products)
            self._consumption =np.zeros_like(self._consumption)
