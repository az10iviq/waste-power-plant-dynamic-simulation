from config import *

tariff_sun = {
	"power":	0.2291, # EURO/kWh
	"heat":		0.0892, # EURO/kWh
	"oil":		0.0725, # EURO/kWh
}

o2_sun = {
	"cons": 297.04 # Nm^3/h
}

mass_flow_heat_net = 180  # m^3/h

# region gases
gas_constant = 8.314462     # J/K/mol
molar_gas_vol = 0.022413    # Nm^3/mol

h2_params = {
	"h_o":			3.542,					# kWh/Nm^3 upper (german: "oberer") heating value
	"h_u":			2.994,					# kWh/Nm^3 lower (german: "unterer") heating value
	"M":			0.002016,				# kg/mol
	"rho_norm":		0.08988,				# kg/Nm^3
	"kappa":		1.41,					# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
	#"v_dot":		o2_sun["cons"]*8760*2	# Nm^3/a minimum H2 production of electrolyzer **per year!** in order to meet the O2 demand
	}

ch4_params = {
	"h_o":		11.06,	# kWh/Nm^3
	"h_u":		9.97,	# kWh/Nm^3
	"rho_norm":	0.72,	# kg/Nm^3
	"M":		0.02,	# kg/mol
	"cp":		2.226,	# kJ/kg/K
	"kappa":	1.31,	# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
	}

h2o_params = {
	"M":			0.018,		# kg/mol
	"rho_l":		998,		# kg/m^3
	"cp_l":			4.18,		# kJ/kg/K
	"h_evap_5bar":	2104.09,	# kJ/kg
	"h_evap_10bar":	2014.9,		# kJ/kg
	"h_8bar":		2652.23,	# kJ/kg
	"kappa":		1.33,		# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
	}

co2_params = {
	"rho_norm":		1.98,	# kg/Nm^3
	"cp":			0.846,	# kJ/kg/K
	"M":			0.0441,	# kg/mol
	"kappa":		1.30,	# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
	}

n2_params = {
	"M":			0.0147,	# kg/mol
	"rho_norm":		1.25,	# kg/Nm^3
	"cp":			1.04,	# kJ/kg/K
	"kappa":		1.40,	# from https://www.powderprocess.net/Tools_html/Data_Diagrams/Tools_isentropic_coefficients.html
}


biogas_params = {"ch4_frac": 0.6} # fraction of CH4 in Biogas
biogas_params["co2_frac"] =    1 - biogas_params["ch4_frac"]

sng_params = {
	"ch4_frac":	0.95,
	"co2_frac":	0.01,
	"h2_frac":	0.04,
}
# endregion gases

# region initailization params

# CHP
chp_gas_storage_rules_dict = {
    "chp1": {
        # name:   threshold, tendency, gas load
        # above 1200 Nm^3, tendency doesn't matter, full load
        "rule1": [1200, 0, 1],
        "rule2": [800, 0, 0.54]  # above 800 Nm^3, tendency positive, 50% load
    },
    "chp2": {
        # name:   threshold, tendency, gas load
        # above 1200 Nm^3, tendency doesn't matter, full load
        "rule1": [1200, 0, 1],
        "rule2": [800, 0, 0.54]  # above 800 Nm^3, tendency positive, 50% load
    },
    "chp3": {
        # name:   threshold, tendency, gas load
        # above 500 Nm^3, tendency doesn't matter, full load
        "rule1": [500, 0, 1],
        # above 800 Nm^3, tendency doesn't matter, 50% load
        "rule2": [400, 0, 0.55]
    },
    "chp4": {
        # name:   threshold, tendency, gas load
        # above 1000 Nm^3, tendency doesn't matter, full load
        "rule1": [1000, 0, 1],
        # above 800 Nm^3, tendency doesn't matter, 50% load
        "rule2": [800, 0, 0.55]
    }
}

chp_efficiency_rules_dict = {
    "chp1": {
        # name: gas load, eta_el, eta_th
        "load0":    [0, 0, 0],
        "load0.54": [0.54, 0.373, 0.494],
        "load1":    [1, 0.408, 0.448]
    },
    "chp2": {
        # name: gas load, eta_el, eta_th
        "load0":    [0, 0, 0],
        "load0.54": [0.54, 0.373, 0.494],
        "load1":    [1, 0.408, 0.448]
    },
    "chp3": {
        # name: gas load, eta_el, eta_th
        "load0":    [0, 0, 0],
        "load0.55": [0.55, 0.364, 0.431],
        "load1":    [1, 0.404, 0.425]
    },
    "chp4": {
        # name: gas load, eta_el, eta_th
        "load0":    [0, 0, 0],
        "load0.55": [0.55, 0.364, 0.431],
        "load1":    [1, 0.404, 0.425]
    }
}

# TODO: Check if the value is calculated for upper or lower heating value
chp_max_gas_power_uptake_dict = {
    "chp1": 2086,  # kW
    "chp2": 2086,  # kW
    "chp3": 1302,  # kW
    "chp4": 1302,  # kW
}

chp_failures_dict = {
    # name: avg occurence, duration
    # fails in average every 144 hours, duration of failure/maintenance 12 hours
    "chp1": [144, 12],
    # fails in average every 124 hours, duration of failure/maintenance 16 hours
    "chp2": [124, 16],
    # fails in average every 171 hours, duration of failure/maintenance 10 hours
    "chp3": [171, 10],
    # fails in average every 98 hours, duration of failure/maintenance 35 hours
    "chp4": [98, 35],
}

# set maintenance schedule for CHPs as a dict of dicts (one dict for each CHP, including dict for maintenance rules)
chp_maintenance_rules_dict = {

    "chp1": {
        # name: interval, duration
        "rule1": [1026, 25],  # every 1000 hours, maintenance duration 5 hours
        "rule2": [100000, 5],  # one more rule for maintenance duration 5 hours
        # just another rule to show the capability of the LoadScheduler
        "rule3": [100000, 5],
    },
    "chp2": {
        "rule1": [1052, 14],
    },
    "chp3": {
        "rule1": [2786, 16],
    },
    "chp4": {
        "rule1": [1048, 18],
    }
}

# Boiler
boiler_max_gas_power_uptake_dict = {
    "boiler1": 2448,  # kW of gas
    "boiler2": 2448  # kW of gas
}

boiler_efficiency_rules_dict = {
    "boiler1": {
        # name: gas load, eta_th
        "load0":    [0, 0],
        "load0.3":  [0.3, 0.9],
        "load1":    [1, 0.9]
    },
    "boiler2": {
        # name: gas load, eta_th
        "load0":    [0, 0],
        "load0.3":  [0.3, 0.9],
        "load1":    [1, 0.9]
    }
}

boiler_stepless_intervals_dict = {
    "boiler1": {
        # intervals for stepless control of boilers
        # name, lower bound, upper bound
        "0.3-1":    [0.3, 1]
    },
    "boiler2": {
        # name, lower bound, upper bound
        "0.3-1":    [0.3, 1]
    }
}

# electrolyzer
electrolyzer_efficiency_rules_dict = {
    "el1": {
        # name: el_load, eta_ho, eta_th (here, it is just the surplus heat from electrolysis)
        "load0":    [0, 0.7969, 0.2031],
        "load1":    [1, 0.7969, 0.2031]
    },
}

electrolyzer_stepless_intervals_dict = {
    "el1": {
        # intervals for stepless control of electrolyzer
        # name, lower bound, upper bound
        "0-1":    [0, 1]
    },
}

max_el_power_uptake = {
    "el1": 2700,  # kW of electricity
}
electrolyzer_efficiency_rules, _, num_electrolyzers = get_rules(
    electrolyzer_efficiency_rules_dict)
electrolyzer_stepless_intervals, _, _ = get_rules(
    electrolyzer_stepless_intervals_dict)

electrolyzer_init_args = {
	"max_el_power_uptake": 2700, # kW of electricity
	"electrolyzer_efficiency_rules": electrolyzer_efficiency_rules[0], # see wwtp.py
	"electrolyzer_stepless_intervals": electrolyzer_stepless_intervals[0], # see wwtp.py
	"capex_sp": 0.5, # 0.5 €/kW
	"opex_factor": 0.2, # 10% of capex
	"failure_rate": 0,  # lambda in Poisson's Distribution in [(# of failures) / year]
	"maintenance_time_on_failure": 4, # in days
    "total_simulation_steps": TOTAL_STEPS,
}

methanation_init_args = {
    "meth_max_biogas_power_uptake_dict": 1, # see wwtp.py
    "methanation_efficiency_rules": 1, # see wwtp.py
    "methanation_stepless_intervals": 1, # see wwtp.py
    "maintenance_time_on_failure": 1, # see wwtp.py
    "maintanance_cost_hour": 1, # see wwtp.py
    "capex_sp": 1 # see wwtp.py
    
}



# endregion