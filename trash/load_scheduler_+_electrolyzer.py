from import_setup import *
from wwtp import *
from Electrolyzer import *


chps_list = chps
boilers_list = boilers

# TODO: In the future, only instances of the gas utilities (chps, boilers,...) should be imported, and no more attributes (i.e. we will replace chp_gas_storage_rules and chp_maintenance_rules by simply importing the chps-List (as a numba-List)). This will declutter the code.

# class for the operator - schedules production (according to price and gas storage volume) and maintenance of CHPs
# in order to make it compatible to numba, all self.-variable types are defined with annotations right at the beginning of the class (all other type annotations are ignored by numba, also the ones in the __init__ method. See https://numba.readthedocs.io/en/stable/user/jitclass.html)
# numba has troubles dealing with Lists. So often, using Tuples instead will fix the problem.
# Also, when defining new arrays inside, don't use Numpys dtype argument, as Numba will automatically choose a C-datatype, which is not the same as Numpys dtypes.
# nb.instance.jitclass.CHP
# numba.experimental.jitclass.base.Boiler


class LoadScheduler:
    heat_net: HeatNet
    def __init__(self, \
        flare_threshold: float, \
        chp_gas_storage_rules: np.ndarray, \
        chp_maintenance_rules: np.ndarray, \
        chps: List, \
        boilers: List, \
        electrolyzer: Electrolyzer, \
        heat_net: HeatNet, \
        price_threshold: float):
        """
        TODO: please improve docstring on this class definition (e.g. explain methods shortly...)
        Arguments:
            flare_threshold: float, the gas storage volume from which the flare is starting to run
            chp_gas_storage_rules: numpy array (dimensions: (n_chps, n_rules, 2)) containing the load rules based on gas storage for each CHP. Last dimension: element 0: gas storage volume threshold, element 1: load
            chp_maintenance_rules: numpy array (dimensions: (n_chps, n_rules, 2)) containing the maintenance rules for each CHP. Last dimension: element 0: runtime threshold, element 1: maintenance duration
            chps: List, Numba-List of CHP objects
            boilers: List, Numba-List of boiler objects
            heat_net: HeatNet, HeatNet instance
            price_threshold: threshold for the price below which the CHPs can run
        """
        # creating some local variables
        self.num_chps = len(chps)
        self.flare_threshold = flare_threshold
        self.chp_maintenance_rules = chp_maintenance_rules
        self.chp_gas_storage_rules = chp_gas_storage_rules
        self.price_threshold = price_threshold

        # check if all rule-tables are addressing the same number of CHPs
        if self.num_chps != len(self.chp_maintenance_rules):
            raise ValueError("chp_maintenance_rules does not match the number of CHPs")
        if self.num_chps != len(self.chp_gas_storage_rules):
            raise ValueError("chp_gas_storage_rules does not match the number of CHPs")

        # Helper variable to hold number of valid (not NaN) maintenance rules for each CHP. Has shape (n_chps,)
        self.num_maintenance_rules = np.count_nonzero(~np.isnan(self.chp_maintenance_rules[:, :, 0]), axis=1)
        # Helper variable to count the time a maintenance rule is active.
        self.maintenance_time = np.zeros((self.num_chps, self.chp_maintenance_rules.shape[1]))
        # Helper variable to count the time the CHP is active between two maintenance intervals. Has shape (n_chps,n_rules)
        self.run_time_between_maintenance = np.zeros((self.num_chps, self.chp_maintenance_rules.shape[1]))

        # Helper variable to check if gas storage rules are active. Has shape (n_chps, n_rules)
        # self.is_gas_storage = np.zeros((self.num_chps, chp_gas_storage_rules.shape[1]))
        # Helper variable to hold number of valid (not NaN) gas storage rules for each CHP. Has shape (n_chps,)
        self.num_gas_storage_rules = np.count_nonzero(~np.isnan(self.chp_gas_storage_rules[:, :, 0]), axis=1)
        # Helper variable to indicate if specific gas storage rules are active. Has shape (n_chps, n_rules)
        self.gas_storage_above_threshold = np.zeros(shape=(self.num_chps, chp_gas_storage_rules.shape[1]))

        # because the operator needs to know how much impact the CHPs can produce, we need to pass the chps List to the operator
        # this instance is a plain copy of the boilers List at the beginning of the simulation. It is not updated during the simulation.
        self.chps = chps
        
        self.electrolyzer: Electrolyzer = electrolyzer

        # because the operator needs to know how much impact the boilers and CHPs have on the heat network, we need to pass the heat network to the operator
        # this instance is a plain copy of the heat network, so that the operator can change the load of the boilers and CHPs without changing the heat network
        self.heat_net = heat_net
    
        # because the operator needs to know how much heat the boilers can produce, we need to pass the boilers List to the operator
        # this instance is a plain copy of the boilers List at the beginning of the simulation. It is not updated during the simulation.
        self.boilers = boilers
        self.num_boilers = len(boilers)


        # Helper variable to decide if it is economically feasible to run the CHPs
        self.price_below_threshold : bool = False


        self.set_gas_storage_rules(chp_gas_storage_rules)
        self.set_maintenance_rules(chp_maintenance_rules)




    def schedule_production(self, chp_old_run_time : np.ndarray, old_maintenance : np.ndarray, old_gas_storage : float, new_time_diff : float, new_price : float, maintenance_remaining : np.ndarray, flare_old_run_time: float, boiler_old_run_time: np.ndarray, old_heat_net.temp: float, mass_flow_heat_net: float, temp_threshold: float):
        """
        TODO: improve docstring on this function
        Since maintenance and price are only implemented for CHPs, the variables are not named specifically for CHPs (e.g. old_maintenance is implicitly the old maintenance of the CHPs)
        Might have to be changed in the future (different price thresholds for CHPs and Electrolyzers, different maintenance rules for CHPs and Electrolyzers)
        ## schedule_production
        ### Takes arguments:
            chp_old_run_time : np.ndarray of dimension (n_chps,)
            old_maintenance : np.ndarray of dimension (n_chps, )
            old_gas_storage : float
            new_time_diff : float
            new_price : float
            maintenance_remaining : np.ndarray of dimension (n_chps,)
            flare_old_run_time: float
            boiler_old_run_time: np.ndarray of dimension (n_boilers,)
            old_temp_heat_net: float
            mass_flow_heat_net: float
            temp_threshold: float, threshold for the temperature below which the boilers need to start (in °C)

        ### Returns:
            chp_new_run_time
            new_maintenance
            chp_load
            self.maintenance_time
            maintenance_remaining
            self.gas_storage_above_threshold
            self.price_below_threshold
            self.run_time_between_maintenance
            boiler_load
            boiler_new_run_time
            flare_new_run_time
            flare_gas_consumption
            
        """

        # self.chp_maintenance_rules dimensions: (n_chps, n_rules, 2) - last dimension: element 0: runtime threshold, element 1: maintenance duration
        # self.chp_gas_storage_rules dimensions: (n_chps, n_rules, 2) - last dimension: element 0: gas storage volume threshold, element 1: load
        chp_new_run_time = chp_old_run_time
        new_maintenance = old_maintenance
        flare_new_run_time = flare_old_run_time
        new_heat_net.temp = old_heat_net.temp
        boiler_new_run_time = boiler_old_run_time

        # reset the gas load of all CHPs
        # Helper variable to show gas load of chps and boilers (**from the planning horizon**). Has shape (n_chps,)
        chp_load = np.zeros(self.num_chps)
        boiler_load = np.zeros(self.num_boilers)

        # Helper variables to model heat production of the CHPs and boilers
        chp_heat_production = 0.0
        boiler_heat_production = 0.0

        # TODO: Put this loop inside a separate method "is_maintenance_needed" to declutter the code
        # check if maintenance is needed because the run_time exceeds the maintenance rules. If so, set the maintenance_remaining to the maintenance duration
        # i refers to the CHPs
        # j refers to the rules
        for i in range(self.num_chps):
            # num_maintenance_rules has shape (num_chps,)
            for j in range(self.num_maintenance_rules[i]):
                # if the run_time is larger than the maintenance rule and this maintenance rule is not yet active, set the maintenance_remaining to the maintenance duration
                # chp_maintenance_rules dimensions: (n_chps, n_rules, 2) - last dimension: element 0: runtime threshold, element 1: maintenance duration
                # TODO: put all print arguments in a debug mode log level to save computation time
                # logging.debug("checking if the time has come for maintenance")
                # logging.debug(f"self.run_time_between_maintenance[{i},{j}] >= self.chp_maintenance_rules[{i},{j},0]): " + str(self.run_time_between_maintenance[i,j] >= self.chp_maintenance_rules[i,j,0]))
                # logging.debug(f"(self.maintenance_time[{i},{j}] % self.chp_maintenance_rules[{i},{j},1] == 0): " + str((self.maintenance_time[i,j] % self.chp_maintenance_rules[i,j,1] == 0)))
                if (self.run_time_between_maintenance[i,j] >= self.chp_maintenance_rules[i,j,0]) & (self.maintenance_time[i,j] % self.chp_maintenance_rules[i,j,1] == 0):
                    # logging.debug("time for maintenance")
                    maintenance_remaining[i] += self.chp_maintenance_rules[i,j,1]
                else:
                    # logging.debug("no maintenance")
                    pass
        
        # TODO: Put this loop inside a separate method "maintenance" to declutter the code
        # decide if maintenance should be done or not
        # if the maintenance_remaining is larger than 0, increase maintenance time (new_maintenance) and decrease maintenance_remaining
        for i in range(self.num_chps):
            if maintenance_remaining[i] > 0:
                # logging.debug("Doing Maintenance")
                new_maintenance = old_maintenance + new_time_diff
                maintenance_remaining[i] -= new_time_diff
                self.run_time_between_maintenance[i,:] = 0
                self.maintenance_time[i,:] += new_time_diff
            else:
                new_maintenance = old_maintenance

                

        # TODO: Put this statement inside a separate method "is_price_below_threshold" to declutter the code
        # decide if price is below threshold
        if new_price < self.price_threshold:
            self.price_below_threshold = True
        else:
            # logging.debug("Price too high")
            self.price_below_threshold = False
        
        # TODO: Put this statement inside a separate method "is_gas_storage_above_threshold" to declutter the code
        # decide if gas storage is above threshold so that CHPs can run
        for i in range(self.num_chps):
            # Reverse the range of rules to start with the least important rule
            for j in np.arange(self.num_gas_storage_rules[i]-1,-1,-1):
                if old_gas_storage > self.chp_gas_storage_rules[i,j,0]:
                    self.gas_storage_above_threshold[i,j] = True
                    chp_load[i] = self.chp_gas_storage_rules[i,j,2]
                        
                else:
                    self.gas_storage_above_threshold[i,j] = False
            if not np.any(self.gas_storage_above_threshold[i,:]):
                # logging.debug("Gas Storage too low")
                pass

        # based on the above decisions, decide if CHPs can run
        # if run, set chp_new_runtime to chp_old_run_time + new_time_diff
        # if not run, set chp_new_runtime to chp_old_run_time
        # regarding load: find largest possible gas load that is above the gas load threshold (True)

        for i in range(self.num_chps):
            # if price is below threshold, gas storage is above any threshold and no maintenance rule is active, run CHP
            # logging.debug("checking if i can run based on the upper conditions")
            # logging.debug(f"self.price_below_threshold: " + str(self.price_below_threshold))
            # logging.debug(f"np.any(self.gas_storage_above_threshold[{i},:])" + str(np.any(self.gas_storage_above_threshold[i,:])))
            # logging.debug(f"maintenance_remaining[{i}] == 0: " + str(maintenance_remaining[i] == 0))
            # logging.debug(f"((self.maintenance_time[{i},:self.num_maintenance_rules[{i}]] % self.chp_maintenance_rules[{i},:self.num_maintenance_rules[{i}],1]).any() == False)" + str(((self.maintenance_time[i,:self.num_maintenance_rules[i]] % self.chp_maintenance_rules[i,:self.num_maintenance_rules[i],1]).any() == False)))
            # & ((self.maintenance_time[i,:self.num_maintenance_rules[i]] % self.chp_maintenance_rules[i,:self.num_maintenance_rules[i],1]).any() == False)
            if (self.price_below_threshold) & (np.any(self.gas_storage_above_threshold[i,:])) & (maintenance_remaining[i] >= 0):
                # logging.debug("i can run")
                chp_new_run_time[i] = chp_old_run_time[i] + new_time_diff
                self.run_time_between_maintenance[i,:] += new_time_diff
                helper_heat, _ = self.chps[i].calculate_output(chp_load[i])
                chp_heat_production += helper_heat
            else:
                # logging.debug("i cannot run")
                chp_new_run_time[i] = chp_old_run_time[i]
                chp_load[i] = 0
        # logging.debug("new_run_time: " + str(chp_new_run_time))
        # logging.debug("new_maintenance: " + str(new_maintenance))
        # logging.debug("self.run_time_between_maintenance: " + str(self.run_time_between_maintenance))
        # logging.debug("chp_heat_production: " + str(chp_heat_production))

        # decide if flare must run
        # if old_gas_storage is above threshold: flare must run
        # if run, set flare_new_run_time to flare_old_run_time + new_time_diff
        # if not run, set flare_new_run_time to flare_old_run_time
        if old_gas_storage >= self.flare_threshold:
            # TODO: This should be new_time_diff = avg_time_diff
            if new_time_diff == 0:
                new_time_diff = 0.1
            flare.gas_consumption = (old_gas_storage - self.flare_threshold) / new_time_diff # Nm^3 / h, prevent division by zero
            flare_new_run_time = flare_old_run_time + new_time_diff
        else:
            flare.gas_consumption = 0.0
            flare_new_run_time = flare_old_run_time
        
        # TODO: Put this statement inside a separate method "calculate_heat_needed" to declutter the code
        # now calculate if the temperature of the heat network would be below the threshold
        new_heat_net.temp = self.heat_net.new_temperature(chp_heat_production, old_heat_net.temp, mass_flow_heat_net)
        # logging.debug("new_temp_heat_net: " + str(new_temp_heat_net))
        # if new_temp_heat_net is below temp_threshold, additional heat must be produced by boilers
        if new_heat_net.temp < temp_threshold:
            # logging.debug("Temperature of heat net would be too low")
            # calculate how much heat must be produced by boilers
            additional_heat_needed = - self.heat_net.calculate_heat(temp_threshold, new_heat_net.temp, mass_flow_heat_net)
            # logging.debug("heat_needed: " + str(heat_needed))
            # TODO: put this statement inside separate method "calculate_boiler_load" to declutter the code
            # now calculate the boiler load
            for i in range(self.num_boilers):
                boiler_load[i] = self.boilers[i].calculate_load(additional_heat_needed)
                boiler_heat_production = self.boilers[i].calculate_output(boiler_load[i])
                additional_heat_needed -= boiler_heat_production
                boiler_new_run_time[i] = boiler_old_run_time[i] + new_time_diff
                # avoid unnecessary calculations
                if additional_heat_needed < 1e-3:
                    break
        
        # TODO: A function should be implemented here to control the Emergency Coolers (if temperature of heat network is too high)

            

        return chp_new_run_time, new_maintenance, chp_load, self.maintenance_time, maintenance_remaining, self.gas_storage_above_threshold, self.price_below_threshold, self.run_time_between_maintenance, boiler_load, boiler_new_run_time, flare_new_run_time, flare.gas_consumption

    def set_gas_storage_rules(self, chp_gas_storage_rules):
        """
        Re-Set the gas storage rules for the CHPs after initialization
        """

        self.chp_gas_storage_rules = chp_gas_storage_rules
        # Helper variable to hold number of valid (not NaN) gas storage rules for each CHP. Has shape (n_chps,)
        self.num_gas_storage_rules = np.count_nonzero(~np.isnan(self.chp_gas_storage_rules[:, :, 0]), axis=1)
        # Helper variable to indicate if specific gas storage rules are active. Has shape (n_chps, n_rules)
        self.gas_storage_above_threshold = np.zeros(shape=(self.num_chps, chp_gas_storage_rules.shape[1]))

        # check if all rule-tables are addressing the same number of CHPs
        if self.num_chps != len(self.chp_maintenance_rules):
            raise ValueError("chp_maintenance_rules and chp_gas_storage_rules must address the same number of CHPs")

    def set_maintenance_rules(self, chp_maintenance_rules):
        """
        Re-Set the maintenance rules for the CHPs after initialization
        """
        # Helper variable to hold number of valid (not NaN) maintenance rules for each CHP. Has shape (n_chps,)
        self.num_maintenance_rules = np.count_nonzero(~np.isnan(chp_maintenance_rules[:, :, 0]), axis=1)
        # Helper variable to count the time a maintenance rule is active.
        self.maintenance_time = np.zeros((self.num_chps, chp_maintenance_rules.shape[1]))
        # Helper variable to count the time the CHP is active between two maintenance intervals. Has shape (n_chps,n_rules)
        self.run_time_between_maintenance = np.zeros((self.num_chps, chp_maintenance_rules.shape[1]))

    def schedule_electrolyzer(self, old_run_time, old_maintenance_time, o2_demand, new_time_diff):
        """
            Defines the Load of the Electrolyzer module and keeps track of its Maintenance
        """
        if  old_maintenance_time > 0:
            self.electrolyzer_info[MAINTENANCE] = old_maintenance_time - new_time_diff
            load = 0
            self.electrolyzer_info[NEW_RUNTIME] = old_run_time
        else:
            self.check_electrolyzer_maintenance()
            if self.electrolyzer_info[MAINTENANCE] == 0:
                load = electrolyzer.calculate_load(o2_demand)
                self.electrolyzer_info[NEW_RUNTIME] = old_run_time + new_time_diff
                self.electrolyzer_info[TIME_SINCE_LAST_MAINTENANCE] += new_time_diff
            else:
                load = 0
                self.electrolyzer_info[TIME_SINCE_LAST_MAINTENANCE] = 0
                self.electrolyzer_info[NEW_RUNTIME] = old_run_time
        
        return load, self.electrolyzer_info[NEW_RUNTIME], self.electrolyzer_info[MAINTENANCE]

    def check_electrolyzer_maintenance(self):
        print(self.electrolyzer_info[TIME_SINCE_LAST_MAINTENANCE], electrolyzer_maintenance_rules[0, 0, OPERATION_TIME])
        if self.electrolyzer_info[TIME_SINCE_LAST_MAINTENANCE] > electrolyzer_maintenance_rules[0, 0, OPERATION_TIME]:
            self.electrolyzer_info[MAINTENANCE] = electrolyzer_maintenance_rules[0, 0, MAINTENANCE_TIME]

    def update_economic_info(self):
        balance = methanation.profit() - methanation.cost() - fermenter.cost() - electrolyzer.cost()
        return balance


    