# For now, this file is not used in the main script. It is used to test Numba and coupling with optimization.ipynb

from import_setup import *
from ..archive.load_scheduler import *

@njit
def loop(
    time_arr: np.ndarray,
    time_diff: np.ndarray,
    raw_price: np.ndarray,
    gas_production: np.ndarray,
    heat_demand_fermenter: np.ndarray,
    chp_run_time: np.ndarray,
    chp_load: np.ndarray,
    chp_el_power: np.ndarray,
    chp_th_power: np.ndarray,
    chp_gas_consumption: np.ndarray,
    maintenance: np.ndarray,
    maintenance_time_per_rule: np.ndarray,
    gas_storage: np.ndarray,
    flare_run_time: np.ndarray,
    flare_gas_consumption: np.ndarray,
    temp_heat_net: np.ndarray,
    boiler_load: np.ndarray,
    boiler_th_power: np.ndarray,
    boiler_run_time: np.ndarray,
    boiler_gas_consumption: np.ndarray,
    operator: LoadScheduler,
    chps: List,
    flare: Flare,
    boilers: List,
    heat_net: HeatNet,
    biogas_h_u: float,
    num_chps: int,
    num_boilers: int,
    maintenance_remaining_failure: np.ndarray,
    mass_flow_heat_net: float,
):
    """
    Loops over the time array and calls the scheduler and the CHPs
    Arguments:


        time_arr: np.ndarray of time steps
        time_diff: np.ndarray of time differences between simulation steps
        raw_price: np.ndarray of raw gas prices
        gas_production: np.ndarray of gas production
        heat_demand_fermenter: np.ndarray of heat demand of fermenter
        chp_run_time: np.ndarray of run times of the CHPs
        maintenance: np.ndarray of maintenance times of the CHPs
        chp_load: np.ndarray of load of the CHPs
        maintenance_time_per_rule: np.ndarray of maintenance times of the CHPs
        flare_run_time: np.ndarray of run times of the flare
        flare_gas_consumption: np.ndarray of gas consumptions of the flare
        gas_storage: np.ndarray of gas storage levels
        maintenance_remaining_failure: np.ndarray of maintenance time remaining of the CHPs
        num_chps: int, number of CHPs
        chp_el_power: np.ndarray of electrical power output of the CHPs
        chp_th_power: np.ndarray of thermal power output of the CHPs
        chp_gas_consumption: np.ndarray of gas consumptions of the CHPs
        operator: Operator object (must be jitclass)
        chps: list of CHP objects (must be jitclass)
        flare: Flare object (must be jitclass)
        biogas_h_u: float, lower heating value of biogas

        time_arr: np.ndarray, time steps
        time_diff: np.ndarray, time differences between simulation steps
        raw_price: np.ndarray, raw gas prices
        gas_production: np.ndarray, gas production
        heat_demand_fermenter: np.ndarray, heat demand of fermenter
        chp_run_time: np.ndarray, run times of the CHPs
        chp_load: np.ndarray, load of the CHPs
        chp_el_power: np.ndarray, electrical power output of the CHPs
        chp_th_power: np.ndarray, thermal power output of the CHPs
        chp_gas_consumption: np.ndarray, gas consumptions of the CHPs
        maintenance: np.ndarray, maintenance times of the CHPs
        maintenance_time_per_rule: np.ndarray, time the maintenance rules will be applied
        gas_storage: np.ndarray, gas storage levels
        flare_run_time: np.ndarray, run times of the flare
        flare_gas_consumption: np.ndarray, gas consumptions of the flare
        temp_heat_net: np.ndarray, temperature of the heat net
        boiler_load: np.ndarray, load of the boilers
        boiler_th_power: np.ndarray, thermal power output of the boilers
        boiler_run_time: np.ndarray, run times of the boilers
        boiler_gas_consumption: np.ndarray, gas consumptions of the boilers
        operator: LoadScheduler, operator object
        chps: List, list of CHP objects
        flare: Flare, flare object
        boilers: List, list of boiler objects
        heat_net: HeatNet, heat net object
        biogas_h_u: float, lower heating value of biogas
        num_chps: int, number of CHPs
        num_boilers: int, number of boilers
        maintenance_remaining_failure: np.ndarray, maintenance time remaining of the CHPs
        mass_flow_heat_net: float, mass flow of the heat net
    Returns:
        time_arr: np.ndarray of time steps
        chp_run_time: np.ndarray of run times of the CHPs
        maintenance: np.ndarray of maintenance times of the CHPs
        chp_load: np.ndarray of load of the CHPs
        maintenance_time_per_rule: np.ndarray of maintenance times of the CHPs
        flare_run_time: np.ndarray of run times of the flare
        flare_gas_consumption: np.ndarray of gas consumptions of the flare
        gas_storage: np.ndarray of gas storage levels
        time_diff: np.ndarray of time differences between simulation steps
        raw_price: np.ndarray of raw gas prices
        maintenance_remaining_failure: np.ndarray of maintenance time remaining of the CHPs
        num_chps: int, number of CHPs
        chp_el_power: np.ndarray of electrical power output of the CHPs
        chp_th_power: np.ndarray of thermal power output of the CHPs
        chp_gas_consumption: np.ndarray of gas consumptions of the CHPs
        gas_production: np.ndarray of gas production
    """
    # chp_load = np.copy(chp_load)
    for k in range(1, len(time_arr)):
        # old_run_time=np.array([1,1]), old_maintenance=np.array([0,0]), old_gas_storage=1200, new_time_diff=1, new_price=0.1
        # run scheduler
        logging.debug("now calculating timestep: " + str(k))

        # calculate new temperature of heat network based on heat demand of fermenter
        temp_heat_net[k] = heat_net.new_temperature(heat_demand_fermenter[0],temp_heat_net[k-1],mass_flow_heat_net)
        # schedule runnning of chps and flare (and boilers)
        chp_run_time[k,:], maintenance[k,:], chp_load[k,:], maintenance_time_per_rule[k,:], _, _, _,\
            run_time_between_maintenance, boiler_load[k,:], boiler_run_time[k,:], flare_run_time[k], flare_gas_consumption[k] = \
            operator.schedule_production(chp_run_time[k-1,:], maintenance[k-1,:],gas_storage[k-1], time_diff[k], raw_price[k], maintenance_remaining_failure, flare_run_time[k-1], boiler_run_time[k-1,:], temp_heat_net[k], mass_flow_heat_net, 80)
        # run CHPs
        for i in range(num_chps):
            chp_load[k,i], chp_el_power[k,i], chp_th_power[k,i], chp_gas_consumption[k,i], maintenance_remaining_failure[i], chp_run_time[k,:] = \
                    chps[i].run(chp_load[k,i], run_time_between_maintenance[i,:], maintenance_remaining_failure[i], chp_run_time[k,:], time_diff[k], biogas_obj.h_u)
        # run flare
        flare_gas_consumption[k] = flare.run(flare_gas_consumption[k])

        # run Boilers
        for i in range(num_boilers):
            boiler_load[k,i], boiler_th_power[k,i], boiler_gas_consumption[k,i], boiler_run_time[k,i] = \
                boilers[i].run(boiler_load[k,i], boiler_run_time[k,i], time_diff[k], biogas_obj.h_u)
        
        # calculate new temperature of heat network based on heat demand of fermenter
        temp_heat_net[k] = heat_net.new_temperature(np.sum(boiler_th_power[k,:])+np.sum(chp_th_power[k,:]),temp_heat_net[k-1],mass_flow_heat_net)

        # calculate new gas storage.
        # TODO: For now, the gas storage can be computed in main. If future complexity is coming in (if some costs are associated with it etc...) the gas storage needs to be separated into another object
        gas_storage[k] = gas_storage[k-1] + (gas_production[k] - chp_gas_consumption[k,:].sum() - flare_gas_consumption[k]) * time_diff[k] # in Nm^3

        # every 1000 timesteps, print the current timestep as info log level
        if k % 10000 == 0:
            # logging.info("timestep: " + str(k) + " of " + str(len(time_arr)))
            pass
        # For debugging, print a timestep near the problem to stop sim there
        if time_arr[k] == 7100*4:
            # logging.debug("timestep: " + str(k) + " of " + str(len(time_arr)))
            pass
    # print(np.sum(chp_load,axis=None))
    return time_arr, time_diff, raw_price, gas_production, heat_demand_fermenter, chp_run_time, chp_load, chp_el_power, chp_th_power, chp_gas_consumption, maintenance, maintenance_time_per_rule, gas_storage, flare_run_time, flare_gas_consumption, temp_heat_net, boiler_load, boiler_th_power, boiler_run_time, boiler_gas_consumption, operator, chps, flare, boilers, heat_net, biogas_h_u, num_chps, num_boilers, maintenance_remaining_failure, mass_flow_heat_net