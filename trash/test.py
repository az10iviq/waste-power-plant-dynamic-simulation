from import_setup import *
from archive.electrolysis import *
from modules.methanation import *
from init import *

flare_threshold = 6500 # Nm^3 (1800 is the safety limit of the 3000 Nm^3 capacity of gas holder 1). If both tanks are available (8000 Nm^3 total volume), the threshold is 6500 Nm^3


# TODO: in the future, these variables can also be handled by a separate function/script
# for easier handling, different dataframes are created for the most needed quantities
# for type safety, the variables extracted from df are all specified as float arrays
# variables that are written into a new line every timestep
time_arr = df["Time / h"].values.astype(float)
time_diff = df["Time_diff / h"].values.astype(float)
raw_price = df["Price / EURO/MWh"].values.astype(float)
gas_production = df["Gas Production / Nm^3/h"].values.astype(float)
heat_demand_fermenter = df["Fermenter Heat Demand / kW"].values.astype(float)
chp_run_time = np.ones((time_arr.shape[0], num_chps))
chp_load = np.zeros((time_arr.shape[0], num_chps))
chp_el_power = np.zeros((time_arr.shape[0], num_chps))
chp_th_power = np.zeros((time_arr.shape[0], num_chps))
chp_gas_consumption = np.zeros((time_arr.shape[0], num_chps))
maintenance = np.zeros((time_arr.shape[0], num_chps))
maintenance_time_per_rule = np.zeros((time_arr.shape[0], num_chps, num_maintenance_rules))
gas_storage = np.zeros(time_arr.shape)
flare_run_time = np.zeros(time_arr.shape[0])
flare_gas_consumption = np.zeros(time_arr.shape[0])
temp_heat_net = np.zeros(time_arr.shape[0])
boiler_load = np.zeros((time_arr.shape[0], num_boilers))
boiler_th_power = np.zeros((time_arr.shape[0], num_boilers))
boiler_run_time = np.zeros((time_arr.shape[0], num_boilers))
boiler_gas_consumption = np.zeros((time_arr.shape[0], num_boilers))

# variables that are overwritten every timestep
run_time_between_maintenance = np.zeros([num_chps, chp_maintenance_rules.shape[1]], dtype=float)
maintenance_remaining_failure = np.zeros(num_chps, dtype=float)



# initialize operator
from archive.load_scheduler import *
operator = LoadScheduler(flare_threshold, chp_gas_storage_rules, chp_maintenance_rules, chps, boilers, heat_net, price_threshold=80)
gas_storage[0] = 2402.79 # Nm^3
temp_heat_net[0] = 70 # °C


logging.debug("Starting simulation")
# Start the stopwatch
tic = time.time()

for k in range(1, len(time_arr)):
    # old_run_time=np.array([1,1]), old_maintenance=np.array([0,0]), old_gas_storage=1200, new_time_diff=1, new_price=0.1
    # run scheduler
    logging.debug("now calculating timestep: " + str(k))

    # calculate new temperature of heat network based on heat demand of fermenter
    temp_heat_net[k] = heat_net.new_temperature(heat_demand_fermenter[0],temp_heat_net[k-1],mass_flow_heat_net)
    # schedule runnning of chps and flare (and boilers)
    chp_run_time[k,:], maintenance[k,:], chp_load[k,:], maintenance_time_per_rule[k,:], _, _, _,\
        run_time_between_maintenance, boiler_load[k,:], boiler_run_time[k,:], flare_run_time[k], flare_gas_consumption[k] = \
        operator.schedule_production(chp_run_time[k-1,:], maintenance[k-1,:],gas_storage[k-1], time_diff[k], raw_price[k], maintenance_remaining_failure, flare_run_time[k-1], boiler_run_time[k-1,:], temp_heat_net[k], mass_flow_heat_net, 80)
    # run CHPs
    for i in range(num_chps):
        chp_load[k,i], chp_el_power[k,i], chp_th_power[k,i], chp_gas_consumption[k,i], maintenance_remaining_failure[i], chp_run_time[k,:] = \
                chps[i].run(chp_load[k,i], run_time_between_maintenance[i,:], maintenance_remaining_failure[i], chp_run_time[k,:], time_diff[k], biogas_obj.h_u)
    # run flare
    flare_gas_consumption[k] = flare.run(flare_gas_consumption[k])

    # run Boilers
    for i in range(num_boilers):
        boiler_load[k,i], boiler_th_power[k,i], boiler_gas_consumption[k,i], boiler_run_time[k,i] = \
            boilers[i].run(boiler_load[k,i], boiler_run_time[k,i], time_diff[k], biogas_obj.h_u)
    
    # calculate new temperature of heat network based on heat demand of fermenter
    temp_heat_net[k] = heat_net.new_temperature(np.sum(boiler_th_power[k,:])+np.sum(chp_th_power[k,:]),temp_heat_net[k-1],mass_flow_heat_net)

    # calculate new gas storage.
    # TODO: For now, the gas storage can be computed in main. If future complexity is coming in (if some costs are associated with it etc...) the gas storage needs to be separated into another object
    gas_storage[k] = gas_storage[k-1] + (gas_production[k] - chp_gas_consumption[k,:].sum() - flare_gas_consumption[k]) * time_diff[k] # in Nm^3

    # every 1000 timesteps, print the current timestep as info log level
    if k % 10000 == 0:
        logging.info("timestep: " + str(k) + " of " + str(len(time_arr)))

    # For debugging, print a timestep near the problem to stop sim there
    if time_arr[k] == 7100*3:
        logging.debug("timestep: " + str(k) + " of " + str(len(time_arr)))

# Stop the stopwatch
toc = time.time()

logging.info('Simulation took {:3.2f}s to compute, with {} steps'.format(toc-tic, len(time_arr)))